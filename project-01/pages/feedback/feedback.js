Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs: [{
      id: 0,
      value: "体验问题",
      isActive: true
    },
    {
      id: 1,
      value: "商品、商家投诉",
      isActive: false
    }]
  },
  //标题点击事件 从子组件传递父组件
  handleTabsItemChange(e) {
    console.log(e);
    //获取被点击的索引
    const {
      index
    } = e.detail;
    // console.log(e);
    // 修改原数组
    let {
      tabs
    } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false);
    //赋值tabs
    this.setData({
      tabs
    })
  },
})