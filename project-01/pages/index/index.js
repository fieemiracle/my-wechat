// index.js
//引入 用来发送请求 路径需要写全
import {request} from "../../request/index.js";

// 获取应用实例
const app = getApp()

Page({
  data: {
    //轮播图
    swiperList: [],
    //导航
    catesList:[],
    //楼层
    floorList:[]
  },
  onLoad: function (options) {
    //1、发送异步请求来获取轮播图数据
    // wx.request({
    //   url: 'https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata',
    //   success:(result)=>{
    //     console.log(result);
    //     this.setData({
    //       swiperList:result.data.message
    //     })
    //   }
    // })  
    this.getSwiperList();
    this.getCatesList();
    this.getFloorList();
  },
  //获取轮播图数据
  getSwiperList(){
    request({ 
      url: "/home/swiperdata" 
    })
    .then(result => {
      this.setData({
        swiperList: result
      })
    })
    //.then 链式 可避免回调地狱
  },
  //获取导航数据
  getCatesList(){
    request({ url: "/home/catitems" })
    .then(result => {
      this.setData({
        catesList: result

      })
    })
  },
   //获取楼层数据
   getFloorList(){
    request({ url: "/home/floordata" })
    .then(result => {
      this.setData({
        floorList: result
      })
    })
  }
  // ,
  //   getUserProfile(e) {
  //     // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
  //     wx.getUserProfile({
  //       desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
  //       success: (res) => {
  //         console.log(res)
  //         this.setData({
  //           userInfo: res.userInfo,
  //           hasUserInfo: true
  //         })
  //       }
  //     })
  //   },
  //   getUserInfo(e) {
  //     // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
  //     console.log(e)
  //     this.setData({
  //       userInfo: e.detail.userInfo,
  //       hasUserInfo: true
  //     })
  //   }
})
