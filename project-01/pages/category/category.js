// pages/category/category.js
//引入 用来发送请求 路径需要写全
import {
  request
} from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //左侧菜单
    leftMenu: [],
    //右侧列表
    rightList: [],
    //左侧被点击的下标
    currentIndex: 0,
    //右侧导航置顶
    scrollTop: 0
  },
  //接口返回数据
  Cates: [],

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    /*
    web中本地存储和小程序的U币额
    web:localStorage.setItem("key","value") localStorage.getItem("key")
    小程序：wx.setStorageSync("key","value") wx.getStorageSync("key")
    1 先判断一下本地存储中有没有旧的数据
      {time:Date.now(),data:[...]}
    2 没有旧数据 直接发送新请求
    3 有旧的数据 同时 旧的数据也没有过期 就使用本地存储中的旧数据即可
 */
    //获取本地存储中的数据 (小程序中也是存在本地存储 技术)
    const Cates = wx.getStorageSync("cates");
    // 2 判断 
    if (!Cates) {
      // 不存在 发送请求获取数据
      this.getCates();
    } else {
      //有旧数据  定义过期时间
      if (Date.now() - Cates.time > 1000 * 10) {
        //重新发送请求
        this.getCates();
      } else {
        //可以使用旧数据
        this.Cates = Cates.data;

        //构造左侧的大菜单数据
        let leftMenu = this.Cates.map(v => v.cat_name);
        //构造右侧的大菜单数据
        let rightList = this.Cates[0].children;
        this.setData({
          leftMenu,
          rightList
        })
      }
    }

  },
  async getCates() {
    // request({
    //     url: "/categories"
    //   })
    //   .then(result => {
    //     // console.log(result);
    //     this.Cates = result.data.message;

    //     //把接口数据存入到本地存储中
    //     wx.getStorageSync("cates", {
    //       time: Date.now(),
    //       data: this.Cates
    //     })

    //     //构造左侧的大菜单数据
    //     let leftMenu = this.Cates.map(v => v.cat_name);
    //     //构造右侧的大菜单数据
    //     let rightList = this.Cates[0].children;
    //     this.setData({
    //       leftMenu,
    //       rightList
    //     })
    //   })

    //使用es7的async await来发送请求
    const result = await request({
      url: "/categories"
    });
    this.Cates = result;
    //把接口数据存入到本地存储中
    wx.getStorageSync("cates", {
      time: Date.now(),
      data: this.Cates
    })

    //构造左侧的大菜单数据
    let leftMenu = this.Cates.map(v => v.cat_name);
    //构造右侧的大菜单数据
    let rightList = this.Cates[0].children;
    this.setData({
      leftMenu,
      rightList
    })
  },
  handleItemTap(e) {
    console.log(e);
    //获取索引
    //给currentIndex赋值
    //根据不同索引渲染右侧的商品内容
    const {
      index
    } = e.currentTarget.dataset;
    let rightList = this.Cates[index].children;
    this.setData({
      currentIndex: index,
      rightList,
      //重新设置 右侧内容scroll-view标签的距离顶部的距离
      scrollTop: 0
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})