// pages/collect/collect.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    collect:[],
    tabs: [{
      id: 0,
      value: "商品收藏",
      isActive: true
    },
    {
      id: 1,
      value: "品牌收藏",
      isActive: false
    },
    {
      id: 2,
      value: "店铺收藏",
      isActive: false
    },
    {
      id: 3,
      value: "浏览足迹",
      isActive: false
    }
  ]
  },
   //标题点击事件 从子组件传递父组件
   handleTabsItemChange(e) {
    console.log(e);
    //获取被点击的索引
    const {
      index
    } = e.detail;
    // console.log(e);
    // 修改原数组
    let {
      tabs
    } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false);
    //赋值tabs
    this.setData({
      tabs
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onShow() {
    const collect=wx.getStorageSync("collect")|| [];
    console.log('collect='+collect);
    this.setData({
      collect
    });
      
  }
})