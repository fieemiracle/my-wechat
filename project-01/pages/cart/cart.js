/*2 调用小程序内置 api 获取用户的收货地址 wx.chooseAddress 
2 获取 用户 对小程序 所授予 获取地址的权限 状态 scope
1 假设 用户 点击获取收货地址的提示框 确定 authSetting scope.address
scope 值 true
2 假设 用户 点击获取收货地址的提示框 取消
scope 值 false
1 诱导用户 自己 打开 授权设置页面 当用户重新给与获取地址权限的时候2 获取收货地址
3 假设 用户 从来没有调用过 收货地址的api
scope undefined*/

import {
  getSetting,
  chooseAddress,
  openSetting,
  showModal,
  showToast
} from "../../utils/asyncWx.js";
import regeneratorRuntime from '../../lib/runtime/runtime';

Page({
  async handleAddAddress() {
    /*
    这个代码是未使用es7方法的源代码
    wx.getSetting({
      success: (result) => {
           const scopeAddress=result.authSetting["scope.address"];
           if(scopeAddress===true||scopeAddress===undefined){
             wx.chooseAddress({
               success: (result1) => {
                 console.log(result1);
               },
             })
           }else{
           wx.openSetting({
              success: (result2) => {
               wx.chooseAddress({
                 success: (result3) => {
                   console.log(result3);
                 },
               })
              },
            })
           }
         }
    })*/

    //点击 收货地址
    try {
      // 1 获取 权限状态
      const res1 = await getSetting();
      const scopeAddress = res1.authSetting["scope.address"];
      // 2 判断 权限状态
      if (scopeAddress === false) {
        //3 先诱导用户打开授权页面 
        await openSetting();
      }
      // 4 调用获取收货地址的 api
      let address = await chooseAddress();
      address.all = address.provinceName + address.cityName + address.countyName + address.detailInfo;
      // console.log(address);
      // 5 存入缓存中
      wx.setStorageSync("address", address);

    } catch (error) {
      console.log(error);
    }

    /*
    以下两个方法可进行单独分别运行，查看控制台结果，获得想要的存储数据的名字
    handleAddAddress(){
      wx.chooseAddress({
        success: (result) => {
          console.log(result);
        },
      })

      wx-wx.getSetting({
        withSubscriptions: true,
        success: (result) => {
          console.log(result);
        }
      })*/
  },
  data: {
    address: {},
    cart: [],
    allChecked: false,
    totalPrice: 0,
    totalNum: 0
  },
  handleItemChange(e) {
    // 获取被修改的商品的id
    const goods_id = e.currentTarget.dataset.id;
    // console.log(goods_id);
    // 获取购物车数组
    let {
      cart
    } = this.data;
    // 找到被修改的商品对象
    let index = cart.findIndex(v => v.goods_id === goods_id);

    // 选中状态取反
    cart[index].checked = !cart[index].checked;
    this.setCart(cart);
  },

  setCart(cart) {
    // 重新计算
    let allChecked = true;
    let totalPrice = 0;
    let totalNum = 0;
    cart.forEach(v => {
      if (v.checked) {
        totalPrice += v.num * v.goods_price;
        totalNum += v.num;
      } else {
        allChecked = false;
      }
    })
    // 判断数组是否为空
    allChecked = cart.length != 0 ? allChecked : false;
    //2 赋值
    this.setData({
      cart,
      allChecked,
      totalPrice,
      totalNum
    });

    wx.setStorageSync("cart", cart);
  },
  handleItemAllChecked() {
    //全选和反选
    // 1全选复选框绑定事件 change
    // 2 获取 data中的全选变量 allchecked3 直接取反 allChecked=!allChecked
    // 4 遍历购物车数组 让里面 商品 选中状态跟随 allchecked 改变而改变
    // 5 把购物车数组 和 allChecked 重新设置回data 把购物车重新设置回 缓存中
    let {
      cart,
      allChecked
    } = this.data;
    allChecked = !allChecked;
    cart.forEach(v => {
      v.checked = allChecked
    });
    this.setCart(cart);
  },
  async hadleItemNum(e) {
    // 商品数量的编辑
    // 1"+""_"按钮 绑定同一个点击事件 区分的关键 自定义属性
    // 1“+”"+1"2 "-" "-1"
    // 2 传递被点击的商品id goods id
    // 3 获取data中的购物车数组来获取需要被修改的商品对象4 直接修改商品对象的数量 num
    // 5 把cart数组 重新设置回 缓存中 和data中
    let {operation,id}=e.currentTarget.dataset;
    let {cart}=this.data;
    const index=cart.findIndex(v=>v.goods_id=id);
    if(cart[index].num===1 && operation===-1){
      const res=await showModal({content:"确认删除该商品？"});
      if(res.confirm){
        cart.splice(index,1);
        this.setCart(cart);
      }
    }else{
      cart[index].num+=operation;
      this.setCart(cart);
    }
    
  },

  async handlePay(){
    let {address,totalNum}=this.data;
    if(!address.userName){
      const res=await showToast({title:"你还没有授权收货地址！"});
    }else if(totalNum===0){
      const res=await wx.showToast({title:"你还没有选购的商品！" })
    }else{
      wx-wx.navigateTo({
        url: '/pages/pay/pay'
      })
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    //1 获取缓存中的收货地址
    const address = wx.getStorageSync('address');

    //获取缓存中的购物车数据
    const cart = wx.getStorageSync("cart") || [];

    // every用法，数组遍历方法，会遍历 会接受一个回调函数 每一个回调函数都返回TRUE 则every方法的返回值也是TRUE
    // 只要有一个回调函数反悔了FALSE 则不在循环执行 直接返回FALSE
    // 空数组调动every方法 返回值就是TRUE
    // const allChecked = cart.length ? cart.every(v => v.checked) : false;
    this.setCart(cart);
    //2 赋值
    this.setData({
      address
    });
  }
})