//引入 用来发送请求 路径需要写全
import {
  request
} from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsObject: {},
    // 初始状态
    isCollect:false
  },
  // 商品对象
  constPics: {},
  /**
   * 生命周期函数--监听页面加载
   */
  onShow() {
    // onShow没有options参数
    var curPages =  getCurrentPages();
    let currentPages=curPages[curPages.length-1];
    let options=currentPages.options;

    const {
      goods_id
    } = options;
    console.log(goods_id);
    this.getGoodsDetail(goods_id);
  },
  async getGoodsDetail(goods_id) {
    const goodsObject = await request({
      url: "/goods/detail",
      data: {
        goods_id
      }
    });
    console.log(goodsObject);
    this.constPics = goodsObject;

    // 获取缓存中的商品收藏的数组
    let collect=wx.getStorageSync("collect") || [];
    console.log(collect);
    // 判断当前商品是否被收藏
    let isCollect=collect.some(v=>v.goods_id===this.constPics.goods_id);

    this.setData({
      goodsObject: {
        goods_name: goodsObject.goods_name,
        goods_price: goodsObject.goods_price,
        //iphone部分手机不识别webp
        // 找后台 进行修改
        // 也可以临时自己修改 确保后台存在.webp格式的图片
        goods_introduce: goodsObject.goods_introduce.replace(/\.webp/g, '.jpg'),
        pics: goodsObject.pics
      },
      isCollect
    })
  },
  handleImagePre(e) {
    const urls = this.constPics.pics.map(v => v.pics_mid);
    const current = e.currentTarget.dataset.url;
    wx.previewImage({
      urls,
      current,
      showmenu: true,
    })
  },
  handleCartClick() {
    //1 获取缓存中的购物车 数组的形式
    let cart = wx.getStorageSync("cart")||[];
    // 判断 商品对象是否已经存在购物车数组中
    let index = cart.findIndex(v => v.goods_id === this.constPics.goods_id);
    if (index === -1) {
      // 不存在 第一次添加
      this.constPics.num = 1;
      this.constPics.checked=true;
      cart.push(this.constPics);
    } else {
      //已经存在购物车 执行num++
      cart[index].num++;
    }
    //把购物车重新添加到缓存中
    wx.setStorageSync("cart", cart);
    //弹出按提示
    wx.showToast({
      title: '成功加入购物车',
      icon: 'success',
      //防止用户持续点击加入购物车按钮
      mask: true,
    });

  },
  // // 商品收藏:页面收藏的时候加载缓存中商品收藏的数据
  // // 判断商品是否被收藏
  // // 单击收藏按钮时判断商品是否存在于缓存收藏商品数组
  // onShow() {
  //   // onShow没有options参数
  //   var curPages =  getCurrentPages();
  //   let currentPages=curPages[curPages.length-1];
  //   let options=currentPages.options;
    
  // },
  
  handleCollectBtn(){
    let isCollect=false;
    // 获取缓存中的商品数组
    let collect=wx.getStorageInfoSync("collect") || [];
    console.log(collect);
    // 判断商品是否被收藏过
    let index=collect.findIndex(v=> v.goods_id===this.constPics.goods_id);
    
    // 如果index!==-1  则说明被收藏过了
    if(index!==-1){
      // 能找到 删除商品
      collect.splice(index,1);
      isCollect=false;
      wx.showToast({
        title: '取消成功',
        icon: 'success',
        mask: true
      });
        
    }else{
      collect.push(this.constPics)
      isCollect=true;
      wx.showToast({
        title: '收藏成功',
        icon: 'success',
        mask: true
      });
    }
    wx.setStorageSync('collect', collect);
      this.setData({
        isCollect
      })
  }
})