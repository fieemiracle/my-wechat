import {
  login
} from "../../utils/asyncWx.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
import {
  request
} from '../../request/index.js';
Page({
  data: {},
  // async handleGetUserInfo(e) {
  //   try {
  //     // console.log(e);
  //     // 获取用户信息
  //     const {
  //       encryptedData,
  //       rawData,
  //       iv,
  //       signature
  //     } = e.detail;
  //     // 获取code
  //     const {
  //       code
  //     } = await login();
  //     const loginParams = {
  //       encryptedData,
  //       rawData,
  //       iv,
  //       signature,
  //       code
  //     };
  //     // 获取token
  //     const {
  //       token
  //     } = await request({
  //       url: "/users/wxlogin",
  //       data: loginParams,
  //       method: "post"
  //     });
  // const token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjIzLCJpYXQiOjE1NjQ3MzAwNzksImV4cCI6MTAwMTU2NDczMDA3OH0.YPt-XeLnjV-_1ITaXGY2FhxmCe4NvXuRnRB8OMCfnPo";
  //     // token存入缓存
  //     wx.setStorageSync("token", token);
  //     wx.navigateBack({
  //       data: 1
  //     });
  //   } catch (error) {
  //     console.log(error);
  //   }
  // },
  async handleGetUserInfo(e) {
    try {
      // 1 获取用户信息 
      const {
        encryptedData,
        rawData,
        iv,
        signature
      } = e.detail;
      // 2 获取小程序登录成功后的code 
      const {
        code
      } = await login();
      const loginParams = {
        encryptedData,
        rawData,
        iv,
        signature,
        code
      };
      // 3 发送请求 获取用户的token
      const token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjIzLCJpYXQiOjE1NjQ3MzAwNzksImV4cCI6MTAwMTU2NDczMDA3OH0.YPt-XeLnjV-_1ITaXGY2FhxmCe4NvXuRnRB8OMCfnPo";
      // const {token} = await request({url: "/users/wxlogin", data: loginParams, method: "post"});
      // 4 吧token存储到缓存中，同时跳转回上一页面
      wx.setStorageSync("token", token);
      wx.navigateBack({
        delta: 1 //1表示返回上一层，2表示返回上2层 
      });
    } catch (error) {
      console.log(error);
    }

  }
})
  