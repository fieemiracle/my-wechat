//引入 用来发送请求 路径需要写全
import {
  request
} from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
Page({
  data: {
    goods:[],
    isFoucs:false,
    inpValue:""
  },
  TimeId:-1,
  handleValueSearch(e){
    console.log(e);
    const {value}=e.detail;
    if(!value.trim()){
      this.setData({
        goods:[],
        isFoucs:false
      });
      // 值不合法
      return;
    }
    // 防抖（一般用于输入框，重复输入，重复发送请求）
    // 节流（一般用在页面下拉和上拉）
    clearTimeout(this.TimeId);
    this.TimeId=setTimeout(()=>{
      this.qsearch(value);
    },1000);
     // 准备发送请求获取数据
    this.qsearch(value);
    this.setData({
      isFoucs:true
    })
  },
  async qsearch(query){
    const res=await request({url:"/goods/qsearch",data:{query}});
    console.log(res);
    this.setData({
      goods:res
    })
  },
  handleCancel(){
    this.setData({
      inpValue:"",
      isFoucs:false,
      goods:[]
    })
  }
})