//引入 用来发送请求 路径需要写全
import {
  request
} from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs: [{
        id: 0,
        value: "综合",
        isActive: true
      },
      {
        id: 1,
        value: "销量",
        isActive: false
      },
      {
        id: 2,
        value: "价格",
        isActive: false
      }
    ],
    goodsList: []
  },
  QueryParams: {
    query: "",
    cid: "",
    pagenum: 1,
    pagesize: 10
  },
  totalPages: 1,
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // console.log(options);//返回上个页面的cid
    this.QueryParams.cid = options.cid;
    this.getQueryParams();

    // wx.showLoading({
    //   title: '加载中',
      
    // });
    // setTimeout(function () {
    //   wx.hideLoading();
        
    // },5000)
      
  },

  // es7方法 异步编程 async await
  async getQueryParams() {
    const result = await request({
      url: "/goods/search",
      data: this.QueryParams
    });
    console.log(result);
    const total = result.total;
    this.totalPages = Math.ceil(total / this.QueryParams.pagesize);
    console.log(this.totalPages);
    this.setData({
      goodsList: [...this.data.goodsList,...result.goods]
    })

    //关闭下拉刷新得窗口
    wx.stopPullDownRefresh();
  },
  //标题点击事件 从子组件传递父组件
  handleTabsItemChange(e) {
    console.log(e);
    //获取被点击的索引
    const {
      index
    } = e.detail;
    // console.log(e);
    // 修改原数组
    let {
      tabs
    } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false);
    //赋值tabs
    this.setData({
      tabs
    })
  },

  onReachBottom() {
    //判断是否还有下一页
    if (this.QueryParams.pagenum >= this.totalPages) {
      wx.showToast({
        title: '没有更多数据了'
      });
      console.log("没有更多数据");
    } else {
      this.QueryParams.pagenum++;
      this.getQueryParams();
      console.log("有更多数据");
    }
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    this.setData({
      goodsList: []
    })
    this.QueryParams.pagenum = 1;
    this.getQueryParams();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },



  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})