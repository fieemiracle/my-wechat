import {
  request
} from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
Page({
  orders:[],
  data: {
    tabs: [{
      id: 0,
      value: "全部",
      isActive: true
    },
    {
      id: 1,
      value: "待付款",
      isActive: false
    },
    {
      id: 2,
      value: "待收货",
      isActive: false
    },
    {
      id: 3,
      value: "退款/退货",
      isActive: false
    }
    ],
  },
  onShow() {
    // 页面被打开的时候 onShow()
    // 获取url上的参数type
    // 根据type去发送请求获取订单数据
    // 渲染页面
    // 点击不同的标题 重新发送请求来获取订单数据
    // onLoad(options){console.log(options)}不同于onShow,onShow无法在形参上接受options参数
    // 使用onShow需要先获取当前下程序的页面栈-数组  长度最大是10页面

    // 下面这个token也可以不写，因为我写html时，token默认存在，没有不存在的情况
    const token=wx.getStorageSync('token');
    if(!token){
      wx.navigateTo({
        url: '/pages/auth/auth'
      });
      return;
        
    }
    let curPages = getCurrentPages();
    console.log(curPages);
    // 数组中，索引最大的页面就是当前页面
    let currentPage = curPages[curPages.length - 1];
    console.log(currentPage.options);//已经成功获取type
    let {type}=currentPage.options;
    this.getOrder(type)
    this.changeTabsIndex(type-1);
  },
  // 构造获取订单的方法
  async getOrder(type) {
    const res = await request({ url: "/my/orders/all", data: { type } });
    console.log(res);
    this.setData({
      orders:res.orders.map(v=>({...v,create_time_cn:(new Date(v.create_time*1000).toLocaleString())}))
    })
  },
  changeTabsIndex(index){
     // 修改原数组
     let {
      tabs
    } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false);
    //赋值tabs
    this.setData({
      tabs
    });
  },
  handleTabsItemChange(e) {
    console.log(e);
    //获取被点击的索引
    const {
      index
    } = e.detail;
    // console.log(e);
   this.changeTabsIndex(index);
   this.getOrder(index+1)
  }
})