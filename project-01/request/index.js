let ajaxTims = 0; //同时发送异步代码的次数
export const request = (params) => {
  // 判断url中是否带有/my/请求的是私有的路径，带上header,token 
  let header = {
    ...params.header
  };
  if (params.url.includes("/my/")) {
    // 拼接header,带上token 
    header["Authorization"] = wx.getStorageSync('token');
  }
  ajaxTims++;

  //显示加载中
  wx.showLoading({
    title: '加载中',
    mask: true,
  });


  //定义公共的接口部分
  //https://api-hmugo-web.itheima.net/api/public/v1
  const publicUrl = "https://api-hmugo-web.itheima.net/api/public/v1";
  return new Promise((resolve, reject) => {
    wx.request({
      ...params,

      header: header,
      url: publicUrl + params.url,
      success: (result) => {
        resolve(result.data.message);
      },
      fail: (err) => {
        reject(err);
      },
      complete: () => {
        ajaxTims--;
        if (ajaxTims === 0) {
          wx.hideLoading();
        }
      }
    });
  })
} 