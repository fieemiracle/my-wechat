import Notify from '../../miniprogram_npm/@vant/weapp/notify/notify';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    newGroupModal: false,
    groupName: '',
    beforeClose: () => false
  },

 
  onLoad(options) {

  },
  onChange(e) {
    this.setData({
      groupName: e.detail
    })
  },
  showNewGroupModal() {
    this.setData({
      newGroupModal: true
    });
  },
  closeDialog() {
    this.setData({
      newGroupModal: false
    })
  },
  createGroup() {
    if (this.data.groupName === '') {
      Notify({
        message: '请输入名称',
        duration: 2000,
        selector: '#custom-selector'
      })
    } else {
      console.log(123);
      // 把用户输入的内容传给后端
      wx.cloud.callFunction({
        name: 'createGroup',
        data: {
          groupName: this.data.groupName
        },
        success: (res) => {
          console.log(res);
          this.setData({
            groupName: '',
            newGroupModal: false
          })

          Notify({
            message: '新建成功',
            duration: 2000,
            selector: '#custom-selector',
            background: '#28a745'
          })

          setTimeout(() => {
            wx.switchTab({
              url: "/pages/group/group"
            })
          }, 2000)
        },
        fail: () => {

        }
      })
    }
  },

  onShow() {

  },

  
})