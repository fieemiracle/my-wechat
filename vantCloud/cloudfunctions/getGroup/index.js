// 云函数入口文件
const cloud = require('wx-server-sdk')
// 初始化之前,声明云环境id
const env='cloud1-4g42994n5a7bf42f'

cloud.init()

// 声明数据库的环境
const db=cloud.database({env})

// 云函数入口函数
exports.main = async (event, context) => {
  console.log(cloud.getWXContext());
  console.log(cloud.getWXContext().OPENID);
  let openId=cloud.getWXContext().OPENID
  
  let groupList=await db.collection('user-group').where({
    userId:openId
  }).get()
  console.log(groupList);

  let returnResult=[]
  for(let item of groupList.data){
    const oneGroup=await db.collection('group').where({
      _id:item.groupId
    }).get()

    if(oneGroup.data.length>0){
      const userInfo=await db.collection('user').where({
        openId:oneGroup.data[0].createBy
      }).get()

      oneGroup.data[0].createBy=userInfo.data[0]
      oneGroup.data[0].relateUserGroupId=item._id
      returnResult.push(oneGroup.data[0])
    }
  }
  return returnResult
}