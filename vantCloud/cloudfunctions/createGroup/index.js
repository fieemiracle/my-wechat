// 云函数入口文件
const cloud = require('wx-server-sdk')
// 初始化之前,声明云环境id
const env='cloud1-4g42994n5a7bf42f'

cloud.init()

// 声明数据库的环境
const db=cloud.database({env})

// 云函数入口函数
exports.main = async (event, context) => {
  // const wxContext = cloud.getWXContext()

  // return {//给前端的内容
  //   event,
  //   openid: wxContext.OPENID,
  //   appid: wxContext.APPID,
  //   unionid: wxContext.UNIONID,
  // }

  // 获取前端传递的参数
  // console.log(event.groupName);

  // 获取event默认含有的用户信息
  const userInfo=event.userInfo;

  // 往数据库存入数据
  // 1 在数据库创建group衣柜
  // 2 关联group集合，并上传部署
  return await db.collection('group').add({
    data:{
      name:event.groupName,
      createBy:userInfo.openId,
      createTime:new Date(),
      delete:false,
      updateTime:new Date()
    }
  }).then(res=>{
    return db.collection('user-group').add({
      data:{
        userId:userInfo.openId,
        groupId:res._id,
        invalid:false

      }
    })
  })
}