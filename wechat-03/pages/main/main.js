// pages/main/main.js
Page({
  data: {
    isLeft:true,
    place:'江西',
    imgUrls:[
      'https://img2.baidu.com/it/u=3958887380,2114565395&fm=253&fmt=auto&app=138&f=JPEG?w=800&h=500',
      'https://img2.baidu.com/it/u=962568157,4293203328&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=333',
      'https://img2.baidu.com/it/u=3621514829,524091673&fm=253&fmt=auto&app=138&f=JPEG?w=750&h=421',
      'https://img0.baidu.com/it/u=365370552,3527195199&fm=253&fmt=auto&app=120&f=JPEG?w=1422&h=800',
      'https://img2.baidu.com/it/u=3837910847,2462012216&fm=253&fmt=auto&app=120&f=JPEG?w=1280&h=800'
    ],
    movieList: [
      {
        isShow: 1,
        name: "人生大事",
        imgUrl: "https://p0.pipi.cn/mmdb/25bfd671339c7e8ea33139d0476cb0d92908d.jpg?imageMogr2/thumbnail/2500x2500%3E?imageView2/1/w/128/h/180",
        actor: "朱一龙,杨恩又,王戈",
        time: "2022-06-24",
        wantSee: '',
        currentPlay: '今天183家影院放映2184场'
      },
      {
        isShow: 1,
        name: "神探大战",
        imgUrl: "https://p0.pipi.cn/mmdb/25bfd6d771f0fa57e267cb131f671a5889b2d.jpg?imageMogr2/thumbnail/2500x2500%3E?imageView2/1/w/128/h/180",
        actor: "刘青云,蔡卓妍,林峯",
        time: "2022-07-08",
        wantSee: '',
        currentPlay: '今天185家影院放映1867场'
      },
      {
        isShow: 0,
        name: "外太空的莫扎特",
        imgUrl: "https://p0.pipi.cn/mmdb/25bfd671be15bf51baf0ee3a5d06b91bf94c3.jpg?imageMogr2/thumbnail/2500x2500%3E?imageView2/1/w/128/h/180",
        actor: "黄渤,荣梓杉,莫扎特",
        time: "2022-07-15 本周五上映",
        wantSee: '166208',
        currentPlay: ''
      }
    ]
  },
  handleTabChangleHot(){
    console.log('点击热映');
    this.setData({
      isLeft:true
    })
  },
  handleTabChangleWait(){
    console.log('点击待映');
    this.setData({
      isLeft:false
    })
  }
})