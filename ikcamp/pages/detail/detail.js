// pages/detail/detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    detailData:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(options);
    let id = options.contentId;
    this.getArticleDetail(id);
  },
  getArticleDetail(id){
    wx.request({
      url: 'https://www.fastmock.site/mock/e39e6b4180ca7bb2b79e2e56d30b217e/IKcamp/detail',
      method:'GET',
      data:{
        contentId:id
      },
      success:(res)=>{
        console.log(res);
        let content=res.data.data;
        content.formatDate=this.formatDate(content.lastUpdateTime)
        this.setData({
          detailData:content
        })
      },
      fail(err){
        wx.showModal({
          title:'提示',
          content:err
        })
        console.log(err);

      }
    })
  },
  formatDate(str){
    let arr=str.split(' ').slice(0,1);
    let newArr=arr.join().split('-');
    return `${newArr[0]}/${newArr[1]}/${newArr[2]}`
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})