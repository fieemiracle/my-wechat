import util from '../../utils/index'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    articleList: [],
    visited: []
  },

  onLoad: function (options) {
    // 获取本地缓存
    wx.getStorage({
      key: 'visited',
      success: (res) => {
        // console.log(res);
        this.setData({
          visited: res.data
        })
      },
      fail: () => {
        wx.setStorage({
          key: 'visited',
          data: this.data.visited
        })
      }
    })
    // 获取首页文章列表
    util.request({
      url: 'https://www.fastmock.site/mock/e39e6b4180ca7bb2b79e2e56d30b217e/IKcamp/list'
    }).then(res => {
      console.log(res);
      let articleData = res.data
      let articleList = this.formatArticleData(articleData);

      // 判断哪篇文章缓存了
      for (let i = 0; i < articleList.length; i++) {
        let item = articleList[i]
        for (let j = 0; j < item.articles.length; j++) {
          let articleItem = item.articles[j]
          if (this.data.visited.indexOf(articleItem.contentId) !== -1) {
            articleItem.hasVisited = true
          }
        }
      }

      this.setData({
        articleList
      })
      console.log(this.data.articleList);
    })
  },

  formatArticleData(data) {
    let formatData = null
    if (data && data.length) {
      formatData = data.map(group => {
        // group.date
        group.formatDate = this.dateConvert(group.date)
        return group
      })
    }
    return formatData
  },

  dateConvert(dateStr) { // 2022-06-26   2022年06月26日
    if (!dateStr) return
    let arr = dateStr.split('-')
    return `${arr[0]}年${arr[1]}月${arr[2]}日`
  },


  showDetail(e) {
    // console.log(e.currentTarget.dataset.id);
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: `../detail/detail?contentId=${id}`
    })

    // 缓存已读状态
    let oldVisited = this.data.visited
    oldVisited.push(id)
    let result = [...new Set(oldVisited)]
    // console.log(result);
    wx.setStorage({
      key: 'visited',
      data: result
    })

    // wx.getStorage({
    //   key: 'visited',
    //   success(res) {
    //     // console.log(res);
    //     oldVisited.push(...res.data)
    //     let result = [...new Set(oldVisited)]
    //     // console.log(result);
    //     wx.setStorage({
    //       key: 'visited',
    //       data: result
    //     })
    //   },
    //   fail(err) {
    //     // console.log(err);
    //     wx.setStorage({
    //       key: 'visited',
    //       data: oldVisited
    //     })
    //   }
    // })

  },

  onReady() {

  },

  onShow() {

  },

  onReachBottom() {
    let _this = this
    wx.request({
      url: 'https://www.fastmock.site/mock/e39e6b4180ca7bb2b79e2e56d30b217e/IKcamp/list',
      method: 'GET',
      success(res) {
        console.log(res.data.data);
        let articleList = _this.formatArticleData(res.data.data)

        _this.setData({
          articleList: _this.data.articleList.concat(articleList)
        })

        console.log(_this.data.articleList);
      }
    })
  }
})