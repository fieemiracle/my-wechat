// // 将接口请求函数进行二次封装
// const DEFAULT_REQUEST_OPTIONS={
//     url:'',
//     data:{},
//     header:{
//         'Content-Type':'application/json'
//     },
//     method:'GET',
//     dataType:'json'
// }
// // 面向对象编程
// let util={
//     alert(title="提示",content="出错啦，请稍后再尝试！"){
//         if(typeof content ==='object'){
//             content=JSON.stringify(content);//对象转字符串，字符串转对象json.parse()
//         }
//         wx.showModal({
//             title,
//             content
//         });

//     },
//     request(opt){
//         let options=Object.assign({},DEFAULT_REQUEST_OPTIONS,opt);
//         let {url,data,header,method,dataType}=options;

//         return new Promise((resolve,reject)=>{
//            wx.request({
//             url,
//             data,
//             header,
//             method,
//             dataType,
//             success: (result) => {
//                 if(result && result.statusCode==200 && result.data){
//                     resolve(result.data);
//                 }else{
//                     this.alert('提示',result)
//                     reject(result)
//                 }
//             },
//             fail(error){//404
//                 this.alert('提示',error)
//                 reject(error);
//             }
//         });
//         })



//     }
// }

// export default util

const DEFAULT_REQUEST_OPTIONS = {
    url: '',
    data: {},
    header: {
      'Content-Type': 'application/json'
    },
    method: 'GET',
    dataType: 'json'
  }
  
  let util = {
    alert(title = '提示', content = '好像哪里出问题了~请再试一次') {
      if (typeof content === 'object') {
        content = JSON.stringify(content)
      }
      wx.showModal({
        title,
        content
      })
    },
    request(opt) {
      let options = Object.assign({}, DEFAULT_REQUEST_OPTIONS, opt)
      let {
        url,
        data,
        header,
        method,
        dataType
      } = options
      let self = this
      return new Promise((resolve, reject) => {
        wx.request({
          url,
          data,
          header,
          method,
          dataType,
          success(res) {
            if (res && res.statusCode == 200 && res.data) {
              resolve(res.data)
            } else {
              self.alert('提示', res)
              reject(res)
            }
          },
          fail(err) {
            self.alert('提示', err)
            reject(err)
          }
        })
      })
  
  
    }
  }
  
  export default util