Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 顶部导航
    tabs: [{
        id: 0,
        value: '推荐'
      },
      {
        id: 1,
        value: '会员'
      },
      {
        id: 2,
        value: '活动'
      }
    ],
    // 推荐的数组
    recommend: [],

    // 发现课程导航
    titlebar: [{
        id: 0,
        title: '全部'
      },
      {
        id: 1,
        title: '腹肌'
      },
      {
        id: 2,
        title: '全身减脂'
      },
      {
        id: 3,
        title: '瘦肚子'
      },
      {
        id: 4,
        title: '胸肌'
      },
      {
        id: 5,
        title: '大腿'
      },
      {
        id: 6,
        title: '全部'
      },
      {
        id: 7,
        title: '全部'
      },
      {
        id: 8,
        title: '全部'
      },
      {
        id: 9,
        title: '全部'
      }
    ],
    // 发现课程数组
    findClass: [],
    // 切换发现课程的选中状态
    isIndex: 0,

    // 收藏状态
    isSelected: false,
    collectIndex: []
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.request({
      url: 'https://www.fastmock.site/mock/7224291cf063beceaa27956d01e64560/keep/recommend',
      success: (res) => {
        // console.log(res.data);
        const {
          recommend
        } = res.data;
        // console.log(recommend);
        this.setData({
          recommend
        })
      }
    })
    this.findClass();
  },

  findClass() {
    wx.request({
      url: 'https://www.fastmock.site/mock/7224291cf063beceaa27956d01e64560/keep/find',
      success: (res) => {
        // console.log(res.data);
        const findClass = res.data.find;

        wx.getStorage({
          key: 'collected',
          data: this.data.collectIndex,
          success: (res) => {
            // console.log(res);
            this.setData({
              collectIndex: res.data
            })
            console.log(this.data.collectIndex);
          },
          fail: (error) => {
            wx.setStorage({
              key: 'collected',
              data: this.data.collectIndex
            })
          }
        })

        this.setData({
          findClass
        })
        // console.log(findClass);
        this.handleCollect1();
      }
    })
  },
  handleChangActive(e) {
    console.log(e);
    let isIndex = e.currentTarget.dataset.index;
    this.setData({
      isIndex
    })
    console.log(isIndex);
  },
  // handleCollect(e){
  //   console.log(e);
  //   let isSelected=this.data.isSelected;
  //   //获取缓存中的收藏数组
  //   wx.getStorage({
  //     key:'collect',
  //     data:this.data.collect,
  //     success:(res)=>{
  //       this.setData({
  //         collect:res.data
  //       })
  //     },
  //     fail:()=>{
  //       wx.setStorage({
  //         key:'collect',
  //         data:this.data.collect
  //       })
  //     }
  //   })
  //   const collectObject=this.data.findClass.map((value)=>{
  //     return 
  //   })
  //   // 判断视频是否已经收藏，是的话，取消收藏；不是的话，添加到收藏
  //   // 判断商品是否被收藏过
  //   if(isSelected){//收藏过
  //     isSelected=!isSelected;
  //     wx.showToast({
  //       title: '取消成功',
  //       icon: 'success',
  //       mask: true
  //     });
  //   }else{
  //     isSelected=!isSelected;
  //     wx.showToast({
  //       title: '收藏成功',
  //       icon:'success',
  //       mask:true
  //     })
  //   }
  //   this.setData({
  //     isSelected
  //   })
  // },
  handleIsCollect(e) {
    // const {findClass}=this.data;
    // console.log(findClass);
    const arr = this.data.collectIndex

    // console.log(e);
    let {
      id
    } = e.currentTarget.dataset;
    // console.log(id);
    arr[id] = 1 - arr[id];
    // console.log(arr);
    this.setData({
      collectIndex: arr
    })

    

    // 存入缓存
    wx.setStorage({
      key: 'collected',
      data: this.data.collectIndex
    })
  },
  handleCollect1() {
    const collectIndex = new Array(this.data.findClass.length).fill(0);

    this.setData({
      collectIndex
    })
  },
  onShow: function () {


  }
})