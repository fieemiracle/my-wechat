// component/tabbar/tabbar.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tabs:{
      type:Array,
      value:[]
    }
  },
 
  /**
   * 组件的初始数据
   */
  data: {
    isindex:0
  },

  /**
   * 组件的方法列表
   */
  methods: {
    handleChangeItem(e){
     console.log(e);
    //  获取下标
     let index=e.currentTarget.dataset.index;
     this.setData({
       isindex:index
     })
    //  触发父组件事件
      // this.triggerEvent('handleChangeItem',{index})
    }
  }
})
