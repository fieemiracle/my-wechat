// components/Tabs/Tabs.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tabs:{
      type:Array,
      value:[]
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    
  },

  /**
   * 组件的方法列表
   */
  methods: {
    handleTabs(e){
  //     // console.log(e);
  //获取索引
      const {index}=e.currentTarget.dataset;
  //触发父组件中的自定义事件  同时传递数据给父组件
  //this.triggerEvent("父组件自定义事件的名称"，要传递的参数)
  this.triggerEvent("itemChange",{index});
  //     let {tabs}=this.data;
  //     tabs.forEach((v,i) => i===index?v.isActive=true:v.isActive=false);

  //     this.setData({
  //       tabs
  //     })

    }
  },

  /*
  组件生命周期
组件的生命周期，指的是组件自身的一些函数，这些函数在特殊的时间点或遇到一些特殊的框架事件时被自动触发。
其中，最重要的生命周期是 created attached detached ，包含一个组件实例生命流程的最主要时间点。
组件实例刚刚被创建好时， created 生命周期被触发。此时，组件数据 this.data 就是在 Component 构造器中定义的数据 data 。 此时还不能调用 setData 。 
通常情况下，这个生命周期只应该用于给组件 this 添加一些自定义属性字段。
在组件完全初始化完毕、进入页面节点树后， attached 生命周期被触发。此时， this.data 已被初始化为组件的当前值。这个生命周期很有用，
绝大多数初始化工作可以在这个时机进行。
在组件离开页面节点树后， detached 生命周期被触发。退出一个页面时，如果组件还在页面节点树中，则 detached 会被触发。
  */ 
/*生命周期方法可以直接定义在 Component 构造器的第一级参数中。
组件的的生命周期也可以在 lifetimes 字段内进行声明（这是推荐的方式，其优先级最高）。

Component构造器可用于定义组件，调用Component构造器时可以指定组件的属性、数据、方法等。
 定义段     类型        是否必填 描述
 properties Object/Map    否    组件的对外属性，是属性名到属性设置的映射表
 data       Object        否    组件的内部数据，和properties一同用于组件的模板渲染
 observers  Object        否    组件数据字段监听器，用于临听 properties和data的变化，参贝数据监听器
 methods    Object        否    组件的方法，包括事件响应函数和任意的自定义方法，关于事件响应函数的使用，参见组件事件
 created    Function      否    组件生命周期函数，在组件实例刚刚被创建时执行，注意此时不能调用setData，参见组件生命周期
 attached   Function      否     组件生命周期函数，在组件实例进入页面节点树时执行，参见组件生命周期
 ready      Function      否    组件生命周期函数，在组件布局完成后执行，参见组件生命周期
 moved      Function      否    组件生命周期函数，在组件实例被移动到节点树另一个位置时执行，参见组件生命周期
 detached   Function      否    组件生命周期函数，在组件实例被从页面节点树移除时执行，参见组件生命周期
error	      Object        否      Error	每当组件方法抛出错误时执行	2.4.1
*/  
lifetimes: {
  attached: function() {
    // 在组件实例进入页面节点树时执行
  },
  detached: function() {
    // 在组件实例被从页面节点树移除时执行
  },
},
// 以下是旧式的定义方式，可以保持对 <2.2.3 版本基础库的兼容
attached: function() {
  // 在组件实例进入页面节点树时执行
},
detached: function() {
  // 在组件实例被从页面节点树移除时执行
},

/*
组件所在页面的生命周期
还有一些特殊的生命周期，它们并非与组件有很强的关联，但有时组件需要获知，以便组件内部处理。这样的生命周期称为“组件所在页面的生命周期”，
在 pageLifetimes 定义段中定义。其中可用的生命周期包括：

生命周期	参数	描述	最低版本
show	   无	  组件所在的页面被展示时执行	2.2.3
hide	   无	  组件所在的页面被隐藏时执行	2.2.3
resize	Object Size	组件所在的页面尺寸变化时执行
*/
pageLifetimes: {
  show: function() {
    // 页面被展示
  },
  hide: function() {
    // 页面被隐藏
  },
  resize: function(size) {
    // 页面尺寸变化
  }
}
})
