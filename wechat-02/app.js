// app.js
/*
12.1.应用生命周期
 属性           类型      默认值 必填 说明
 onLaunch       function  否    监听小程序初始化。
 onShow         function  否    监听小程序启动或切前台。
 onHide         function  否    监听小程序切后台。
 onError        function  否    错误监听函数。
 onPageNotFound function  否    页面不存在监听函数。
*/
App({
  //1、应用第一次启动的时候就会触发的事件
  onLaunch() {
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    
    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    console.log("onLaunch");
    // error//应用报错测试  onError

    //js的方式来跳转页面 测试 onPageNotFound，但是这招方式不会触发 onPageNotFound事件
    // wx.navigateTo({
    //   url: 'pages/checkbox1/checkbox1',
    // })
  },
  globalData: {
    userInfo: null
  },
  //2、应用被用户看到
  onShow:function(){
    console.log("onShow");
  },
  //3、应用被隐藏 
  onHide(){
    //暂停或者清除定时器
    console.log("onHide");
  },
  //4、应用代码发生报错的时候触发
  onError(){
    console.log("onError");
    console.log(err);
  },
  /*
  5、页面找不到时就触发
  应用第一次启动的时候，若找不到第一个入口页面才会触发
  */
 onPageNotFound(){
   console.log(" onPageNotFound");
   //如果页面不存在了通过js的方式来重新跳转页面  重新调到第二个页面 不能调到tabbar 页面 与导航组件类似
   wx.navigateTo({
     url: 'pages/image/image',
   })
 }
})
