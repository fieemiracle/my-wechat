// pages/radio/radio.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    gender:"",
    items: [
      {value: 'USA', name: '美国'},
      {value: 'CHN', name: '中国', checked: 'true'},
      {value: 'BRA', name: '巴西'},
      {value: 'JPN', name: '日本'},
      {value: 'ENG', name: '英国'},
      {value: 'FRA', name: '法国'},
    ]
  },
  handleChange(e){
    let gender=e.detail.value;
    // console.log(gender);
    this.setData({
      gender
    })
  },
  radioChange(e) {
    console.log('radio发生change事件,携带value值为:', e.detail.value)

    const items = this.data.items;
    for (let i = 0; i < items.length; ++i) {
      items[i].checked = items[i].value === e.detail.value
    }
    this.setData({
      items
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {
    return {
      title: 'radio',
      path: 'page/component/pages/radio/radio'
    }
  }
})