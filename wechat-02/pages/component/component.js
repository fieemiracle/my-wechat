// pages/component/component.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs:[
      {
        id:0,
        name:"首页",
        isActive:true
      },
      {
        id:1,
        name:"我的",
        isActive:false
      },
      {
        id:2,
        name:"关于",
        isActive:false
      },
      {
        id:3,
        name:"更多",
        isActive:false
      }
    ]
  },
    //自定义事件 用来接收子组件传递的数据
    handleItemChange(e){
      //接收传递过来的参数
      const {index}=e.detail;
      console.log(index);
      let {tabs}=this.data;
      tabs.forEach((v,i) => i===index?v.isActive=true:v.isActive=false);
      this.setData({
        tabs
      })
    }
  
})