// pages/pageLifetime/pageLifetime.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   *页面生命周期
    属性                类型    说明
    data              Object 页面的初始数据
    onLoad            function 生命周期回调-监听页面加载
    onShow            function 生命周期回调-监听页面显示
    onReady           function 生命周期回调一监听页面初次演染完成
    onHide            function 生命周期回调-监听页面隐藏
    onUnload          function 生命周期回调-监听页面卸载
    onPullDownRefresh function 监听用户下拉动作
    onReachBottom      function 页面上拉触底事件的处理函数
    onShareAppMessage   function 用户点击右上角转发
    onPageScroll        function 页面滚动触发事件的处理函数
    onResize            function 页面尺寸改变时触发，详见 响应显示区域变化
    onTabltemTap         function 当前是tab 页时，点击 tab时触发
   */
  onLoad(options) {
    //发布异步请求来初始化页面数据
    console.log("onLoad");
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    console.log("onShow");
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    console.log("onReady");
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    console.log("onHide");
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    //navigator 中可以实现关闭当前页面的属性值 既可触发事件 即open-Type=redirect|reLaunch|navigateBack
    console.log("onUnload");
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    console.log("onPullDownRefresh");
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    //页面有滚动时才可以触发
    console.log("onReachBottom");
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {
    console.log("onShareAppMessage");
  },
   /**
   * 只要页面有滚动就可以实现
   */
  onPageScroll(){
    console.log("onPageScroll");
  },
  /**
   * 当前是tab 页时，点击 tab时触发
  */
  onTabltemTap(){
    console.log("onTabltemTap");
  },
   /**
   * 小程序发生横屏和竖屏切换时 触发
   */
  onResize(){
    console.log("reSize");
  }
})