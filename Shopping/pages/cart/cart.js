// pages/cart/cart.js
Page({
  data: {
    carts: [{
        id: 1,
        title: '新鲜芹菜 半斤',
        image: '/image/s5.png',
        num: 4,
        price: 2.99,
        selected: true
      },
      {
        id: 2,
        title: '素米 500g',
        image: '/image/s6.png',
        num: 1,
        price: 8.99,
        selected: false
      },
      {
        id: 3,
        title: '芒果 700g',
        image: '/image/s1.png',
        num: 5,
        price: 9.99,
        selected: true
      },
      {
        id: 4,
        title: '瓜子仁 500g',
        image: '/image/s4.png',
        num: 9,
        price: 7.99,
        selected: false
      }
    ],
    selectAllStatus: true,
    totalPrice: 0

  },
  onLoad(options) {
    setTimeout(() => {
      this.setData({ //如果不是箭头函数，this指向会被定时器干扰，出错
        carts: [{
            id: 1,
            title: '新鲜芹菜 半斤',
            image: '/image/s5.png',
            num: 4,
            price: 2.99,
            selected: true
          },
          {
            id: 2,
            title: '素米 500g',
            image: '/image/s6.png',
            num: 1,
            price: 8.99,
            selected: false
          },
          {
            id: 3,
            title: '芒果 700g',
            image: '/image/s1.png',
            num: 5,
            price: 9.99,
            selected: true
          },
          {
            id: 4,
            title: '瓜子仁 500g',
            image: '/image/s4.png',
            num: 9,
            price: 7.99,
            selected: false
          }
        ]
      });
      this.getTotalPrice();
    }, 1000)
  },

  // 全选和全不选
  handleSelectAll() {
    // 取反
    let selectAllStatus = this.data.selectAllStatus;
    selectAllStatus = !selectAllStatus;

    // 获取carts中的selected的状态
    let carts = this.data.carts;
    for (let i = 0; i < carts.length; i++) {
      carts[i].selected = selectAllStatus
    }

    this.setData({
      selectAllStatus,
      carts: carts //键值一样时可省略
    });
    console.log(this.data.selectAllStatus);
    this.getTotalPrice();
  },
  // 单选和多选
  handleSelectList(e) {
    console.log(e.currentTarget.dataset.index);
    let index = e.currentTarget.dataset.index;
    let selected = `carts[${index}].selected`; //改变数组里的selected值的时候的写法
    this.setData({
      [selected]: !this.data.carts[index].selected
    })
    this.getTotalPrice();
    let carts = this.data.carts;
    for (let i = 0; i < carts.length; i++) {
      if (!carts[i].selected) {
        this.setData({
          selectAllStatus: false
        })
        return;
      } else {
        this.setData({
          selectAllStatus: true
        })
      }
    }
  },
  // 计算总价格
  getTotalPrice() {
    // 访问data中的数组
    let carts = this.data.carts;
    let total = 0;
    for (let i = 0; i < carts.length; i++) {
      if (carts[i].selected === true) {
        total += carts[i].num * carts[i].price;
      }
    }
    this.setData({
      totalPrice: total.toFixed(2)
    })
  },
  // 实现增加数量和减少数量
  handleSelectNum(e) {
    console.log(e.currentTarget.dataset.operation);
    // 获取data中的购物车数据
    let carts = this.data.carts;
    // 获取事件源中的自定义属性
    let operation = e.currentTarget.dataset.operation;
    let id = e.currentTarget.dataset.id;
    // 获取当前点击的位置
    let index = carts.findIndex(v => v.id === id);//也可以在标签中新建属性 data-index="{{index}}"

    // 当减到0的时候，删除商品
    if (carts[index].num === 1 && operation === -1) {
      const infomation = wx.showModal({
        cancelColor: 'silver',
        cancelText: '保留',
        confirmColor: 'red',
        confirmText: '确定删除',
        content: '确定删除此商品',
        showCancel: true,
        title: '温馨提示',
        success: (result) => {
          if (result.confirm) {
            carts.splice(index, 1);
            // 重新赋值
            this.setData({
              carts
            })
          } else if (result.cancel) {
            console.log('取消删除');
          }
        }
      })
    } else {
      // 进行加减
      carts[index].num += operation;
      this.getTotalPrice();
      // 重新赋值
      this.setData({
        carts
      })
    }
  },
  // 实现删除操作
  handleDeletePro(e) {
    console.log(e.currentTarget.dataset.id);
    let carts = this.data.carts;
    let id = e.currentTarget.dataset.id;
    let index = carts.findIndex(v => v.id === id);
    const infomation = wx.showModal({
      cancelColor: 'silver',
      cancelText: '再想想',
      confirmColor: 'green',
      confirmText: '狠心删除',
      content: '确认删除此商品？',
      editable: false,
      showCancel: true,
      title: '温馨提示',
      success: (result) => {
        if (result.confirm) {
          carts.splice(index, 1);
          this.setData({
            carts
          })
        } else if (result.cancel) {
          console.log('取消成功');
        }
      }
    })
  }
})