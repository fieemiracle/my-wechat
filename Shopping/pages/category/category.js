// pages/category/category.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    category: [{
        name: '果味',
        id: 'guowei'
      },
      {
        name: '蔬菜',
        id: 'shucai'
      },
      {
        name: '炒货',
        id: 'chaohuo'
      },
      {
        name: '点心',
        id: 'dianxin'
      },
      {
        name: '粗茶',
        id: 'cucha'
      },
      {
        name: '淡饭',
        id: 'danfan'
      }
    ],
    curIndex: 0,
    toView: "cucha",
    detail: [{
        banner: 'http://img.redocn.com/sheji/20151221/tangguobudingbannerlingshipaipsdfencengsucai_5626296.jpg',
        cate: '果味',
        id: 'guowei',
        detail: [{
            thumb: '../../image/c3.png',
            name: '坚果'
          },
          {
            thumb: '../../image/c1.png',
            name: '瓜子'
          },
          {
            thumb: '../../image/c2.png',
            name: '大米'
          },
          {
            thumb: '../../image/c4.png',
            name: '芹菜'
          }
        ]
      },
      {
        banner: 'http://img.redocn.com/sheji/20151221/tangguobudingbannerlingshipaipsdfencengsucai_5626296.jpg',
        cate: '蔬菜',
        id: 'shucai',
        detail: [{
            thumb: '../../image/c3.png',
            name: '坚果'
          },
          {
            thumb: '../../image/c1.png',
            name: '瓜子'
          },
          {
            thumb: '../../image/c2.png',
            name: '大米'
          },
          {
            thumb: '../../image/c4.png',
            name: '芹菜'
          }
        ]
      },
      {
        banner: 'http://img.redocn.com/sheji/20151221/tangguobudingbannerlingshipaipsdfencengsucai_5626296.jpg',
        cate: '炒货',
        id: 'chaohuo',
        detail: [{
            thumb: '../../image/c3.png',
            name: '坚果'
          },
          {
            thumb: '../../image/c1.png',
            name: '瓜子'
          },
          {
            thumb: '../../image/c2.png',
            name: '大米'
          },
          {
            thumb: '../../image/c4.png',
            name: '芹菜'
          }
        ]
      },
      {
        banner: 'http://img.redocn.com/sheji/20151221/tangguobudingbannerlingshipaipsdfencengsucai_5626296.jpg',
        cate: '点心',
        id: 'dianxin',
        detail: [{
            thumb: '../../image/c3.png',
            name: '坚果'
          },
          {
            thumb: '../../image/c1.png',
            name: '瓜子'
          },
          {
            thumb: '../../image/c2.png',
            name: '大米'
          },
          {
            thumb: '../../image/c4.png',
            name: '芹菜'
          }
        ]
      },
      {
        banner: 'http://img.redocn.com/sheji/20151221/tangguobudingbannerlingshipaipsdfencengsucai_5626296.jpg',
        cate: '粗茶',
        id: 'cucha',
        detail: [{
            thumb: '../../image/c3.png',
            name: '坚果'
          },
          {
            thumb: '../../image/c1.png',
            name: '瓜子'
          },
          {
            thumb: '../../image/c2.png',
            name: '大米'
          },
          {
            thumb: '../../image/c4.png',
            name: '芹菜'
          }
        ]
      },
      {
        banner: 'http://img.redocn.com/sheji/20151221/tangguobudingbannerlingshipaipsdfencengsucai_5626296.jpg',
        cate: '淡饭',
        id: 'danfan',
        detail: [{
            thumb: '../../image/c3.png',
            name: '坚果'
          },
          {
            thumb: '../../image/c1.png',
            name: '瓜子'
          },
          {
            thumb: '../../image/c2.png',
            name: '大米'
          },
          {
            thumb: '../../image/c4.png',
            name: '芹菜'
          }
        ]
      }
    ]
  },
  // 事件绑定应该与声明周期函数同级
  handleSwitchTab(e) {
    console.log('hello');
    console.log(e.currentTarget.dataset.index);
    this.setData({
      curIndex:e.currentTarget.dataset.index,
      toView:e.currentTarget.dataset.id
    });
   
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log('onload');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    console.log('onready');
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    console.log('onshow');
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    console.log('onHide');
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    console.log('onpulldownRefresh');
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    console.log('onReachBottom');
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})