// // components/calendar/calendar.js
// Component({
//   /**
//    * 组件的属性列表
//    */
//   properties: {
//     weekText:{
//       type:Array,
//       value:['周日','周一','周二','周三','周四','周五','周六']
//     }
//   },

//   /**
//    * 组件的初始数据
//    */
//   data: {
//     emptyGridBefore:[31],
//     emptyGridCurrent:[],
//     emptyGridAfter:[1,2,3],
//     year:0,
//     month:0,
//     day:0,
//     format:'',
//     YEAR:0,
//     MONTH:0,
//     DAY:0,
//     title:0,
//   },
//   // 生命周期函数
//   ready:function(){
//     this.today();
//   },
//   /**
//    * 组件的方法列表
//    */
//   methods: {
//     today(){
//       // console.log(123);
//       let date=new Date();
//       let year=date.getFullYear();
//       let month=date.getMonth()+1;
//       let day=date.getDate();
//       // month=month<10?'0'+month:month;
//       // day=day<10?'0'+day:day;
//       // let select=year+'-'+month+'-'+day;
//       let select=year+'-'+this.zero(month)+'-'+this.zero(day);

//       this.setData({
//         year,
//         month,
//         day,
//         date,
//         format:select,
//         YEAR:year,
//         MONTH:month,
//         DAY:day
//       })

//       // 初始化日历
//       this.display(year,month,date);

//     },
//     zero(i){
//       return i<10?'0'+i:i;
//     },
//     display(year,month,date){
//       this.setData({
//         year,
//         month,
//         date,
//         title:year+'年'+month+'月'
//       })

//       this.createDays(year,month);
//       this.createEmptyGrid(year,month);
//     },

//     // 当前月份天数
//     createDays(year,month){
//       let thisMonthDays=[],
//           days=this.getThisMonthDays(year,month);
//       for(let i=1;i<=days;i++){
//         thisMonthDays.push({
//           date:i,
//           dateFormat:this.zero(i),
//           monthFormat:this.zero(month),
//           // 当天星期几
//           week:this.data.weekText[new Date(Date.UTC(year,month-1,i)).getDay()]
//         })
//       }
//       this.setData({
//         thisMonthDays
//       })

//     },

//     // 获取某年某月有几天
//     getThisMonthDays(year,month){
//       // 获取当前月的天数
//       // new Date()第三个参数默认1，就是每个月1号，把它设置为0
//       let days=new Date(year,month+1,0).getDate();
//       return days;
//     },

//     // 当前月份空出天数
//     createEmptyGrid(year,month){
//       let week=new Date(Date.UTC(year,month-1,1)).getDay(),
//           emptyGridBefore=[],
//           emptyGridAfter=[],
//           emptyDays=(week===0?7:week);

//       // 当月天数
//       let thisMonthDays=this.getThisMonthDays(year,month)
//       // 上月天数
//       let preMonthDays=(month-1)<0?this.getThisMonthDays(year-1,11):this.getThisMonthDays(year,month-1);
//       // 前面空出来日期
//       for(let i=1;i<=emptyDays;i++) {
//         emptyGridBefore.push(preMonthDays-(emptyDays-i))
//       }
//       // 后面空出来日期
//       let after=((42-thisMonthDays-emptyDays)-7)>=0?(42-thisMonthDays-emptyDays)-7:(42-thisMonthDays-emptyDays);
//       for(let i=1;i<=after;i++){
//         emptyGridAfter.push(i);
//       }

//       this.setData({
//         emptyGridBefore,
//         emptyGridAfter
//       })

//     }
//   }
// })
// components/calendar/calendar.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    weekText: {
      type: Array,
      value: ['周日', '周一', '周二', '周三', '周四', '周五', '周六']
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    emptyGridsBefore: [],
    thisMonthDays: [], // [{}, {}, {}, ...]
    emptyGridsAfter: [],
    year: 0,
    month: 0,
    date: 0,
    format: '', // 格式化的日期
    YEAR: 0,
    MONTH: 0,
    DATE: 0,
    title: ''
  },

  ready: function () {
    this.today()
  },
  /**
   * 组件的方法列表
   */
  methods: {
    today() {
      let DATE = new Date(),
        year = DATE.getFullYear(),
        month = DATE.getMonth() + 1,
        date = DATE.getDate(),
        select = year + '-' + this.zero(month) + '-' + this.zero(date)

      this.setData({
        year: year,
        month,
        date,
        format: select,
        YEAR: year,
        MONTH: month,
        DATE: date
      })
      // 初始化日历
      this.display(year, month, date)
    },

    display(year, month, date) {
      this.setData({
        year,
        month,
        date,
        title: year + '年' + this.zero(month) + '月'
      })
      this.createDays(year, month)
      this.createEmptyGrids(year, month)
    },

    // 当前月份天数
    createDays(year, month) {
      let thisMonthDays = [],
        days = this.getThisMonthDays(year, month)
      for (let i = 1; i <= days; i++) {
        thisMonthDays.push({
          date: i,
          dateFormat: this.zero(i),
          monthFormat: this.zero(month),
          week: this.data.weekText[new Date(Date.UTC(year, month - 1, i)).getDay()]
        })
      }
      this.setData({
        thisMonthDays
      })
    },

    // 当前月份空出的天数
    createEmptyGrids(year, month) {
      let week = new Date(Date.UTC(year, month - 1, 1)).getDay(),
        emptyGridsBefore = [],
        emptyGridsAfter = [],
        emptyDays = (week == 0 ? 7 : week)

      // 当月天数
      let thisMonthDays = this.getThisMonthDays(year, month)
      // 上月天数
      let preMonthDays = month - 1 < 0 ? this.getThisMonthDays(year - 1, 11) : this.getThisMonthDays(year, month - 1)
      // 前面空出来的日期
      for (let i = 1; i <= emptyDays; i++) {
        emptyGridsBefore.push(preMonthDays - (emptyDays - i))
      }

      // 后面空出来的
      let after = (42 - thisMonthDays - emptyDays) - 7 >= 0 ?
        (42 - thisMonthDays - emptyDays) - 7 :
        (42 - thisMonthDays - emptyDays)
      for (let i = 1; i <= after; i++) {
        emptyGridsAfter.push(i)
      }

      this.setData({
        emptyGridsBefore,
        emptyGridsAfter
      })
    },

    getThisMonthDays(year, month) {
      return new Date(year, month, 0).getDate()
    },

    zero(i) {
      return i >= 10 ? i : '0' + i
    },

    // 下个月
    nextMonth() {
      let month = this.data.month == 12 ? 1 : this.data.month + 1;
      let year = this.data.month == 12 ? this.data.year + 1 : this.data.year;
      this.display(year, month, 0)
    }
  }
})