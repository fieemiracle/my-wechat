// app.js
App({
  onLaunch() {
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // nav-bar
    wx.db={}
    // 读取手机设备型号
    let info=wx.getSystemInfoSync();
    // 电量栏高度
    wx.db.statusBarHeight=info.statusBarHeight;
    if(info.platform==='android'){
      // 导航栏高度
      wx.db.navBarHeight=48
    }else{
      wx.db.navBarHeight=44
    }
    // console.log(info);可得到机型信息
    wx.db.url=(url)=>{
      return `https://m.maoyan.com/ajax${url}`
    }

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
  },
  globalData: {
    userInfo: null
  }
})
