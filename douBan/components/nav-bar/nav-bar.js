// components/nav-bar/nav-bar.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {//父组件给我的数据
    statusBarColor:{
      type:String,
      value:'#fff'
    },
    navBarColor:{
      type:String,
      value:'#fff'
    },
    titleColor:{
      type:String,
      value:'#000'
    },
    title:{
      type:String,
      value:''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    statusBarStyle:'',
    navBarStyle:'',
    topHeight:''
  },

  lifetimes:{
    attached:function(){
      // console.log(123);
      let statusBarStyle=`height:${wx.db.statusBarHeight}px;background-color:${this.data.statusBarColor}`;

      let navBarStyle=`height:${wx.db.navBarHeight}px;background-color:${this.data.statusBarColor};color:${this.data.titleColor}`;

      let topHeight=wx.db.statusBarHeight+wx.db.navBarHeight;

      this.setData({
        statusBarStyle,
        navBarStyle,
        topHeight
      })
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {

  }
})
