// components/ratingStar/ratingStar.js
const STAR_ON = '/assets/img/rating_star_small_on.png'
const STAR_HALF = '/assets/img/rating_star_small_half.png'
const STAR_OFF = '/assets/img/rating_star_small_off.png'
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    score: {
      type: 0,
      observer: function(newVal, oldVal) {  // 观察者方法，监听器
        let stars = [STAR_OFF, STAR_OFF, STAR_OFF, STAR_OFF, STAR_OFF]
        let floor = Math.floor(newVal / 2)

        for (let i = 0; i < floor; i++) {
          stars[i] = STAR_ON
        }

        if (newVal % 2 !== 0) {  // 有小数部分
          stars[floor] = STAR_HALF
        }

        this.setData({
          stars: stars
        })
      }
    },
    iconSize: {
      type: String,
      value: '26rpx'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    stars: [STAR_OFF, STAR_OFF, STAR_OFF, STAR_OFF, STAR_OFF]
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})