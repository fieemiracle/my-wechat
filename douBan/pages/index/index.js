Page({
  data: {
    allMovies: [
      {
        title: "院线热映",
        url: "/movieOnInfoList",
        movies: []
      },
      {
        title: "即将上映",
        url: "/comingList?ci=10&token=&limit=10",
        movies: []
      },
      {
        title: "口碑榜",
        url: "/moreClassicList?sortId=1&showType=3&limit=10",
        movies: []
      },
      {
        title: "最受欢迎",
        url: "/moreClassicList?sortId=0&showType=3&limit=5",
        movies: []
      }
    ]
  },
  loadData(index,params={}) {
    let obj=this.data.allMovies[index];
    let url=wx.db.url(obj.url);

    wx.request({
      url:url,
      data:params,
      header:{'Content-type':'json'},
      success:(res)=>{
        // console.log(res);
        let movies=res.data.movieList || res.data.coming;
        // this.setData({
        //   movies
        // })
        obj.movies=movies;//也可这样修改引用类型内层的数据
        this.setData(this.data);//更新数据源
        console.log(this.data.allMovies[index].movies);
      }
    })
  },
  getCity(callback) {
    wx.getLocation({
      type: 'wgs84',
      altitude: false,
      success: (result) => {
        // console.log(result);

        wx.request({
          url: 'https://api.map.baidu.com/reverse_geocoding/v3',
          data: {
            ak: 'RZo9tLIO8ltgV8gGNLFEvwN1nYuWQ79K',
            output: 'json',
            coordtype: 'wgs84',
            location: `${result.latitude},${result.longitude}`
          },
          success: (result) => {
            // console.log(result.data.result.addressComponent.city);
            let city=result.data.result.addressComponent.city;
            callback && callback(city)
          }
        });

      }
    });
  },
  search() {
    // 发接口请求
    console.log(123);
  },
  onLoad(options) {
    this.getCity((city) => {
      this.loadData(0, {
        city: city,
        apikey: '0df993c66c0c636e29ecbb5344252a4a'
      })
    })
    this.loadData(1);
    // this.loadData(2);
    // this.loadData(3);
  }

})