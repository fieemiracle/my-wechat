// pages/bookcity/bookcity.js
Page({
data:{
  hotData:[],
  classifyData:[]
},
toReading(e){
  // console.log(e.currentTarget.dataset.url);
  wx.navigateTo({
    url: `../chapter/chapter?url=${e.currentTarget.dataset.url}`,
    // success: function(res) {
    //   // 通过 eventChannel 向被打开页面传送数据
    //   res.eventChannel.emit('acceptDataFromOpenerPage', { data: e.currentTarget.dataset.url })
    // }
  })
},
toChapter(e){
  console.log(e);
  wx.navigateTo({
    url: `../chapter/chapter?url=${e.currentTarget.dataset.url}`,
  })
},
onLoad(option){
  wx.showLoading({
    title: '火热加载中',
  })
  wx.cloud.callFunction({
    name:'getList',
    data:{},
    success:(res)=>{
      // console.log(res);
      this.setData({
        hotData:res.result.hotData,
        classifyData:res.result.classifyData
      })
       wx.hideLoading();
      console.log(this.data);
    },
    fail:(err)=>{
      console.log(error);
    }
  })
},
onShow(){

}
})