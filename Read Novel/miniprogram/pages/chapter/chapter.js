Page({
  data: {
    bookData: [],
    pageData: [],
    lastData: [],
    url: '',
    statueBtn: '加入书架',

    // 页数
    pageArray: [],
    pre: '',
    next: '',
    page: 1,
    preAble: false,
    nextAble: false
  },
  onLoad(options) {
    // console.log(options);
    const {
      url
    } = options;
    this.setData({
      url
    })
    this.getList(url);
    this.getStatusBtn(url)
  },
  getList(url) {
    wx.showLoading({
      title: 'Loading...',
    })
    wx.cloud.callFunction({
      name: 'bookList',
      data: {
        url: url
      },
      success: (res) => {
        // console.log(res);
        this.setData({
          bookData: res.result.bookData,
          pageData: res.result.bookData[0].sectionList,
          lastData: res.result.bookData[0].lastNewList,
          pageArray: res.result.pageArray,
          pre: res.result.pre || '',
          next: res.result.next || '',
          preAble: res.result.pre ? false : true,
          nextAble: res.result.next ? false : true,
          page: res.result.next ? +res.result.next.split('/')[2] - 1 : +res.result.pre.split('/')[2] + 1
        })
        // console.log(this.data.bookData);
        // console.log(this.data.pageData);
        // console.log(this.data.lastData);
        wx.hideLoading()
      }
    })
  },
  // 开始阅读
  startReading(e) {
    // console.log(this.data.pageData[0].section);
    // console.log(e.currentTarget.dataset);
    // console.log(e);
    if (!e.currentTarget.dataset.url) { //点击开始阅读 
      wx.getStorage({
        key: this.data.url,
        success: (res) => { //本地有缓存，去到上次阅读的地方
          console.log(res);
          wx.navigateTo({
            url: `../read/read?url=${res.data}&name=${this.data.bookData[0].bookname}&bookUrl=${this.data.url}`,
          })
        },
        fail: (err) => { //本地没有缓存，去到这本书的第一章
          // console.log(err);
          wx.navigateTo({
            url: `../read/read?url=${this.data.pageData[0].href}&name=${this.data.bookData[0].bookname}&bookUrl=${this.data.url}`,
          })
        }
      })
    } else { //点击其他按钮
      let {
        url
      } = e.currentTarget.dataset;
      wx.setStorage({
        key: this.data.url,
        data: url
      })

      wx.navigateTo({
        url: `../read/read?url=${url}&name=${this.data.bookData[0].bookname}&bookUrl=${this.data.url}`,
      })
    }

  },
  // 加入书架
  addBookcase() {
    // 把这本书的详情添加到书架
    wx.cloud.callFunction({
      name: 'addBookcase',
      data: {
        detail: this.data.bookData,
        url: this.data.url
      },
      success: (res) => {
        console.log(res);
        wx.showToast({
          title: res.result.message,
          icon: 'success',
          success: () => {
            this.setData({
              statueBtn: res.result.message == '已添加至书架' ? '从书架移出' : '加入书架'
            })
          }
        })
      }
    })
  },
  // 获取加入书架按钮的状态
  getStatusBtn(url) {
    wx.cloud.callFunction({
      name: 'findBookcase',
      data: {
        url: url
      },
      success: (res) => {
        // console.log(res);
        if (res.result) {
          this.setData({
            statueBtn: '移出书架'
          })
        } else {
          this.setData({
            statueBtn: '加入书架'
          })
        }
      }
    })
  },
  // 跳转我的书架
  toBookcase() {
    wx.switchTab({
      url: '/pages/booksection/booksection',
    })
  },
  // 上一页下一页
  bindPickerchange(e) {
    // console.log(e);
    let page = parseInt(e.detail.value)
    if (page + 1 != this.data.page) {
      this.setData({
        page: page + 1
      })
      this.getList(this.data.pageArray[page].name)
    }
  },
  handlePre() {
    console.log('pre');
    if (this.data.pre == '') { //判断是否存在上一页
      return;
    } else {
      this.getList(this.data.pre)
    }
  },
  handleNext() {
    console.log('next');
    if (this.data.next == '') { //判断是否存在下一页
      return;
    } else {
      this.getList(this.data.next)
    }
  },

})