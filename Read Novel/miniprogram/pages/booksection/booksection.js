Page({
  data: {
    bookcase:[]
  },
  startReading(e){
    console.log(e);
    let {url}=e.currentTarget.dataset;
    wx.navigateTo({
      url: `../chapter/chapter?url=${url}`,
    })
  },
  onShow() {
    wx.cloud.callFunction({
      name:'getBookcase',
      data:{},
      success:(res)=>{
        // console.log(res);
        this.setData({
          bookcase:res.result.getBookcase.data
        })
        console.log(this.data.bookcase);
      }
    })
  }
})