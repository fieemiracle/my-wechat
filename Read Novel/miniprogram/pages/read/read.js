// pages/read/read.js
Page({
  data: {
    // 上一个页面传来的
    url: '',
    name: '',
    bookUrl: '',

    // 数据库传来的
    category: '',
    content: '',
    contentHtml: '',
    pre: '',
    next: ''
  },
  onLoad(options) {
    // console.log(options);
    let {
      url,
      name,
     bookUrl
    } = options
    this.setData({
      url,
      name,
      bookUrl
    })
    this.getContent(url)
  },
  getContent() {
    wx.showLoading({
      title: 'Loading'
    })
    wx.cloud.callFunction({
      name: 'getContent',
      data: {
        url: this.data.url
      },
      success: (res) => {
        // console.log(res);
        this.setData({
          category: res.result.category,
          content: res.result.content,
          contentHtml: res.result.contentHtml,
          pre: res.result.pre || '',
          next: res.result.next || ''
        })
        // console.log(this.data.contentHtml);
        wx.hideLoading()
        wx.pageScrollTo({//滚动上面
          scrollTop:0,
          duration: 300
        })


        // 更新缓存
        wx.setStorage({
          key:this.data.bookUrl,//书本地址
          data:this.data.next//章节地址
        })
      }
    })
  },
  topreChapter() {
    if (this.data.pre != this.data.category) {
      this.setData({
        url: this.data.pre
      })
      this.getContent(this.data.url)
    } else {
      wx.showToast({
        title: '当前是第一章',
        icon: 'none'
      })
    }
  },
  tonextChapter() {
    if (this.data.next != this.data.category) {
      this.setData({
        url: this.data.next
      })
      this.getContent(this.data.url)
      wx.setStorage({
        key:this.data.bookUrl,
        data:this.data.url
      })
    } else {
      wx.showToast({
        title: '当前是最后一章',
        icon: 'none'
      })
    }

  },
  toCatogory() {
    wx.navigateBack({
      delta: 1
    })
  }
})