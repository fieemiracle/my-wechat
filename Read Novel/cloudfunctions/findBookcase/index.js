// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db=cloud.database();

// 云函数入口函数
exports.main = async (event, context) => {
  const openId = cloud.getWXContext().OPENID
  const url=event.url;

  const result=await db.collection('bookcase').where({
    url:url,
    user:openId
  }).get()

  return result.data.length?1:0
}