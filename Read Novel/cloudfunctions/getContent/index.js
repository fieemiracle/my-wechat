// 云函数入口文件
const cloud = require('wx-server-sdk')
const cheerio = require('cheerio') // 操作html结构
const charset = require('superagent-charset') // 解决乱码
const superagent = require('superagent') // 发接口请求
charset(superagent)

cloud.init()
const db=cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  let serverUrl = `https://m.biqiugege8.com/${event.url}`
  const result = await (superagent.get(serverUrl)).charset('gb2312')//防止乱码
  const data = result.text || ""; //防止报错
  const $ = cheerio.load(data);//加载一下这个页面
  // const wxContext = cloud.getWXContext()

  // 章节内容
  let content=$('#chaptercontent').text()
  let contentHtml=$('#chaptercontent').html()
  console.log(content);
  let pre=$('#pb_prev').attr('href')
  let next=$('#pb_next').attr('href')
  let category=$('#pb_mulu').attr('href')

  return {
    event,
    content,
    contentHtml,
    pre,
    next,
    category
    
  }
}