// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db=cloud.database();

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const getBookcase=await db.collection('bookcase').where({
    user:wxContext.OPENID
  }).get()
  console.log(getBookcase);

  return {
    getBookcase
  }
}