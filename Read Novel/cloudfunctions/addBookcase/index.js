// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db=cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();
  const openId=wxContext.OPENID;
  // console.log(event);
  let {detail,url}=event
  const result=await db.collection('bookcase').where({
    url:url
  }).get()
  if(!result.data.length){
    await db.collection('bookcase').add({
      data:{
        user:openId,
        detail:detail,
        url:url
      }
    })
    return {
      message:'已添加至书架'
    }
  }else{
    await db.collection('bookcase').where({
      url:url
    }).remove()
  }
  return {
    message:'已从书架移除'
  }
}