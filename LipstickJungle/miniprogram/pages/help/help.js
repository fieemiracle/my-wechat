// pages/help/help.js
Page({

  data:{
    condition: Math.floor(Math.random()*7+1)//有多少名*多少名 比如有7位 *7+1
  },

  changeMotto: function() {
    var that = this;
    this.interval =setInterval(function () {
      that.changeContent();
      // console.log("轮播请求0.01秒触发一次");9
    }, 10)    //代表0.01秒钟发送一次请求
    
  },
  changeContent: function () {
    var isShow = this.data.condition;
    this.setData({ condition: Math.floor(Math.random()*7+1) })
  },
  endMotto: function() {
    clearInterval(this.interval);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})