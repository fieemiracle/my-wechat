// pages/mine/mine.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show:false,
    userInfo:{},
    hasUserInfo:false,
    currentIndex:0
  },
  onLoad(options) {

  },
  // 显示弹窗
  changeBackground(){
    this.setData({
      show:true
    })
  },
  // 更换头像
  changeAvartar(e){
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res);
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
        // 存入缓存
        wx.setStorage({
          key:"userInfo",
          data:this.data.userInfo
        },{
          key:'hasUserInfo',
          value:this.data.hasUserInfo
        })
      }
    })
  },
  // 关闭弹窗
  onClose(){
    this.setData({
      show:false
    })
  },
  // 切换区
  handleCollect(e){
    console.log(e.currentTarget.dataset.choice);
    this.setData({
      currentIndex:e.currentTarget.dataset.choice
    })
  },
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    wx.getStorage({
      key: 'userInfo',
      success:(res)=>{
        console.log(res);
        if(res){
            this.setData({
              userInfo:res.data,
              hasUserInfo:true
            })
        }
      }
    })
  },
  // 去种草
  toPlant(){
  setTimeout(()=>{
    wx.switchTab({
      url: '../../pages/home/home'
    })
  },1000)
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})