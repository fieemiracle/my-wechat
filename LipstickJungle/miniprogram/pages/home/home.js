// pages/home/home.js
import {
  showModal,
  showToast
} from '../../utils/tools';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    value: '',
    currentIndex: 0,
    isAuth: false,
    tempFilePaths: '',
    currentBrand: 0,
    isCollected:false
  },
  onChange(e) {
    console.log(e.detail);
    this.setData({
      value: e.detail,
    });
  },
  toSearch() {
    // console.log('toSearch');
    wx.navigateTo({
      url: `../search/search?content=${this.data.value}`,
    })

  },
  onLoad(options) {},

  // 控制中间凸显的样式
  handleChange(e) {
    this.setData({
      currentIndex: e.detail.current
    })
  },
  // 获取标签内容
  getLabel(e) {
    this.setData({
      value: e.currentTarget.dataset.content
    })
    if (this.data.value) {
      setTimeout(() => {
        wx.navigateTo({
          url: `../search/search?content=${this.data.value}`,
        })
      }, 1000)
    }
  },
  // 扫一扫功能
  handleScan() {
    wx.scanCode({
      success: function (res) {
        console.log(res);
      },
      fail: function (error) {
        console.log(error);
      }
    })

  },
  // 拍照搜索
  handlePicture() {
    // 授权
    const _this = this
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.camera']) {
          // 用户已经授权
          _this.setData({
            isAuth: true
          })
        } else {
          // 用户还没有授权，向用户发起授权请求
          wx.authorize({
            scope: 'scope.camera',
            success() { // 用户同意授权
              _this.setData({
                isAuth: true
              })
            },
            fail() { // 用户不同意授权
              _this.openSetting().then(res => {
                _this.setData({
                  isAuth: true
                })
              })
            }
          })
        }
      },
      fail: res => {
        console.log('获取用户授权信息失败')
      }
    })
    // 调用内置照相机实现拍照和上传图片
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        _this.setData({
          tempFilePaths: res.tempFilePaths
        })
      }
    })
  },
  handleBrand(event) {
    console.log(event.currentTarget.dataset);
    this.setData({
      currentBrand: event.currentTarget.dataset.currentbrand
    })
  },
  // 收藏
  handleCollect(e){
    console.log(e.target.dataset);
    if(this.data.currentIndex===e.target.dataset.index){
      e.target.dataset.isCollected=!e.target.dataset.isCollected
    }
  },
  onShow() {

  },
  onPullDownRefresh() {

  }
})