Page({
  data: {
    type: "",
    /** 索引栏*/
    scrollTop: 0,
    /** mock客户数据*/
    customerList: [
      {
        "type": "A",
        "items": [
          {
            "brand": "Anna Sui 安娜苏"
          },
          {
            "brand": "ARITAUM 爱茉莉"
          },
          {
            "brand": "Armmani 阿玛尼"
          },
          {
            "brand": "AUPRES 欧珀莱"
          }
        ]
      },
      {
        "type": "B",
        "items": [
          {
            "brand": "Bourjois 妙巴黎"
          },
          {
            "brand": "Burberry 巴宝莉"
          },
          {
            "brand": "Burt's Bees"
          }
        ]
      },
      {
        "type": "C",
        "items": [
          {
            "brand": "Chanel 香奈儿"
          },
          {
            "brand": "Clio 珂莱欧"
          },
          {
            "brand": "Colorkey 珂拉琪"
          },
          {
            "brand": "Catkin 卡婷"
          },
          {
            "brand": "Carsian 卡姿兰"
          }
        ]
      },
      {
        "type": "D",
        "items": [
          {
            "brand": "DEAR DAHILA 黛丽奥"
          },
          {
            "brand": "Dior 迪奥"
          }
        ]
      },
      {
        "type": "E",
        "items": [
          {
            "brand": "Estee Lauder 雅诗兰黛"
          },
          {
            "brand": "Etude House 伊蒂之屋"
          }
        ]
      },
      {
        "type": "F",
        "items": [
          {
            "brand": "梵家"
          },
          {
            "brand": "Fresh 馥蕾诗"
          }
        ]
      },
      {
        "type": "G",
        "items": [
          {
            "brand": "Givenchy 纪梵希"
          },
          {
            "brand": "Gucci 古驰"
          },
          {
            "brand": "Guerlain 娇兰"
          }
        ]
      },
      {
        "type": "H",
        "items": [
          {
            "brand": "韩熙贞"
          },
          {
            "brand": "花西子"
          }
        ]
      },
      {
        "type": "I",
        "items": [
          {
            "brand": "Innisfree 悦诗风吟"
          },
          {
            "brand": "IPSA 茵芙纱"
          }
        ]
      },
      {
        "type": "J",
        "items": [
          {
            "brand": "Judydoll 橘朵"
          }
        ]
      },
      {
        "type": "K",
        "items": [
          {
            "brand": "Kiko"
          }
        ]
      },
      {
        "type": "L",
        "items": [
          {
            "brand": "Lancome 兰蔻"
          },
          {
            "brand": "L'oreal 欧莱雅"
          }
        ]
      },
      {
        "type": "L",
        "items": [
          {
            "brand": "MAC 魅可"
          },
          {
            "brand": "Maybelline 美宝莲"
          },
          {
            "brand": "美康粉黛"
          }
        ]
      },
      {
        "type": "P",
        "items": [
          {
            "brand": "Perfect Diary 完美日记"
          }
        ]
      },
      {
        "type": "Y",
        "items": [
          {
            "brand": "YSL 圣罗兰"
          }
        ]
      },
      {
        "type": "Z",
        "items": [
          {
            "brand": "Zeesea 滋色"
          },
          {
            "brand": "自然堂"
          }
        ]
      }
    ],
  },

  //索引栏发生变化事件	
  onPageScroll(event) {
    this.setData({
      scrollTop: event.scrollTop,
    });
  },
  /**选择客户发生事件 */
  getCustomer: function (event) {
    console.log(event.currentTarget.dataset.brand);
    setTimeout(()=>{
      wx.switchTab({
        url: `../strategy/strategy?brand=${event.currentTarget.dataset.brand}`,
      })
    },1000)
  },
  onLoad() {
    console.log(options);
    this.setData({
      getContext: options.content
    })
    console.log(this.data.getContext);
  },
  onShow() {
    console.log(123);
    // wx.request({
    //   url: 'https://www.fastmock.site/mock/32934d1f0a841c06f74a25bab2b86956/lipstick-jungle/search',
    //   method: 'GET',
    //   success(res) {
    //     console.log(res.data);
    //     this.setData({
    //       customerList:res.data
    //     })
    //   }
    // })
  }
});

