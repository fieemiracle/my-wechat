// 云函数入口文件
const cloud = require('wx-server-sdk')
// 初始化之前,声明云环境id
const env = 'cloud1-4g42994n5a7bf42f'

cloud.init()

// 声明数据库的环境
const db = cloud.database({
  env
})

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const getTomatoes=await db.collection('myTomatoes').where({
    user:wxContext.OPENID
  }).get()
  // console.log(getTomatoes);
  let tomatoesData=getTomatoes.data[0];
  // console.log(tomatoesData);

  return {
    tomatoesData
  }

}