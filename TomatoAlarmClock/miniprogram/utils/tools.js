// promise形式 封装 showModal
export const showModal=({content})=>{
  return new Promise((resolve,reject)=>{
      wx.showModal({
        title:'提示',
        content:content,
          success: (result) => {
              resolve(result);
          },
          fail: (err) => {
              reject(err);
          }
      });
        
  })
}
// promise形式 封装 showToast
export const showToast=({title})=>{
  return new Promise((resolve,reject)=>{
      wx.showToast({
        title: title,
        icon: 'none',
          success: (result) => {
              resolve(result);
          },
          fail: (err) => {
              reject(err);
          }
      });
        
  })
}
// promise形式 封装 login
export const login=()=>{
  return new Promise((resolve,reject)=>{
      wx.login({
        timeout:10000,
        success:(result)=>{
            resolve(result);
        },
        fail:(err)=>{
            reject(err);
        }
      });
        
  })
}