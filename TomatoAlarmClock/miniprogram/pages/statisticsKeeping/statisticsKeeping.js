// 引入echarts
import * as echarts from "../../ec-canvas/echarts";

// const app = getApp();
// 初始化echarts
function initPieCharts(canvas, width, height, dpr) {
  const mychart = echarts.init(canvas, null, {
    width: width,
    height: height,
    devicePixelRatio: dpr
  })
  canvas.setChart(mychart);

  let option = {
    tooltip: { //提示框，鼠标悬浮交互的信息提示
      trigger: 'item'
    },
    legend: [{ //图例
      bottom: '2%',
      itemWidth: 8,
      itemHeight: 8,
      itemGap: 20,
      textStyle: {
        color: '#949494'
      },
      data: [
        {
          value: 0,
          name: '工作',
          itemStyle: {
            color: '#219CDD'
          }
        },
        {
          value: 0,
          name: '学习',
          itemStyle: {
            color: '#1AFA29'
          }
        },
        {
          value: 0,
          name: '思考',
          itemStyle: {
            color: '#FCAC38'
          }
        },
        {
          value: 0,
          name: '写作',
          itemStyle: {
            color: '#1296DB'
          }
        },
        {
          value: 0,
          name: '运动',
          itemStyle: {
            color: '#122279'
          }
        },
        {
          value: 0,
          name: '阅读',
          itemStyle: {
            color: '#D81E06'
          }
        }
      ]
    }
    ],
    series: [{
      name: '番茄闹钟',
      type: 'pie',
      color: '#F0F0F0',
      radius: ['45%', '60%'],
      avoidLabelOverlap: false,
      selectedMode: "single",
      label: {
        show: false,
        position: 'center'
      },
      emphasis: {
        label: {
          show: true,
          fontSize: '20',
          fontWeight: 'bold'
        }
      },
      labelLine: {
        show: false
      },
      data: [{
        value: 1,
        name: '工作'
      },
      {
        value: 2,
        name: '学习'
      },
      {
        value: 0,
        name: '思考'
      },
      {
        value: 0,
        name: '写作'
      },
      {
        value: 0,
        name: '运动'
      },
      {
        value: 0,
        name: '阅读'
      }]
    }]
  };

  mychart.setOption(option);
  return mychart;

};

function initLineChart(canvas, width, height, dpr) {
  const mychart = echarts.init(canvas, null, {
    width: width,
    height: height,
    devicePixelRatio: dpr
  })
  canvas.setChart(mychart);

  let option = {
    xAxis: {
      type: 'category',
      show: true,
      position: 'bottom',
      silent: false,
      axisLine: {//轴线相关
        show: true,
        splitNumber: 5,
        lineStyle: {
          color: '#D9D9D9'
        }
      },
      axisTick: {//刻度线相关
        show: true,
        alignWithLabel: true,//刻度与标签对齐
        interval: 0,
        inside: true,
        lineStyle: {//刻度线
          color: '#FF4258',
          width: 7,
        },
        length: 1
      },
      axisLabel: {//刻度标签
        show: true,
        // 使用函数模板，函数参数分别为刻度数值（类目），刻度的索引
        formatter: function (value, index) {
          let currentdate = new Date();
          let nowdatetime = {
            year: currentdate.getFullYear(),
            month: currentdate.getMonth() + 1,
            day: currentdate.getDate()
          }

          // 计算前n天日期
          function getBeforeTime(n) {
            let getCurrentTime = new Date().getTime();
            let agotime = getCurrentTime - 86400000 * n; //一天的毫秒数为86400000
            let agoDatetime = new Date(agotime);
            let agodate = {
              year: agoDatetime.getFullYear(),
              month: agoDatetime.getMonth() + 1,
              day: agoDatetime.getDate()
            }
            return agodate;
          };

          let weekDatetime=[];
          weekDatetime.unshift(nowdatetime);
          for(let i=0;i<7;i++){
            weekDatetime.unshift(getBeforeTime(i+1))
          }
          if(index==0){
            return `${weekDatetime[0].month}月${weekDatetime[1].day}日`
          }else{
            return `${weekDatetime[index].day}`
          }
        },
        fontSize:11

      },
    },
    yAxis: {
      type: 'value',
      position: 'left',
      splitNumber: 1,
      max: 25 * 6,
      axisLabel: {//刻度相关
        show: false
      },
      lineStyle: {//刻度线
        color: '#D9D9D9'
      },
    },
    lineStyle: {//折线相关
      // color: '#F16130'
      color: '#bfa'
    },
    areaStyle: {//区域填充
      color: '#FEF6F3'
    },
    series: [{
      data: [25, 25, 25, 25, 25, 25, 25, 25],
      type: 'line'
    }]
  }
  mychart.setOption(option);
  return mychart;
};

// 计算当前时间前n天的日期
// function getBeforeTime(n) {
//   let getCurrentTime = new Date().getTime();
//   let agotime = getCurrentTime - 86400000 * n; //一天的毫秒数为86400000
//   let agoDatetime = new Date(agotime);
//   let agodate = {
//     year: agoDatetime.getFullYear(),
//     month: agoDatetime.getMonth() + 1,
//     day: agoDatetime.getDate()
//   }
//   return agodate;
// };
Page({
  data: {
    ecPie: {
      onInit: initPieCharts
    },
    ecLine: {
      onInit: initLineChart
    },
    // 番茄
    dayTomatoesArr: [],
    createTime:0,
    dayTomatoes:0,
    totalTomatoes: 0,
    weekTomatoes: 0,
    averageTomatoes: 0,
    totalDays: 0
  },
  onShareAppMessage(res) {
    return {
      title: '番茄闹钟好用的话，就分享给好友吧~',
      path: '/miniprogram/pages/timeKeeping/timeKeeping',
      success: function () {

      },
      fail: function () {

      }
    }
  },
  onShow(){
    wx.showLoading({
      title: 'Loading...',
    })
    wx.cloud.callFunction({
      name:'getTomatoes',
      data:{},
      success:(res)=>{
        console.log(res);
        this.setData({
          dayTomatoesArr:res.result.tomatoesData.dayTomatoesArr,
          totalTomatoes:res.result.tomatoesData.totalTomatoes
        })
        console.log(this.data.dayTomatoesArr);
        // console.log(this.data.totalTomatoes);
        let dayTomatoesArr=this.data.dayTomatoesArr;
        let totalDays=this.data.totalDays;
        let averageTomatoes=this.data.averageTomatoes;
        totalDays=Math.ceil((dayTomatoesArr[dayTomatoesArr.length-1].createTime-dayTomatoesArr[0].createTime)/86400000);
        averageTomatoes=Math.floor(this.data.totalTomatoes/totalDays)
        this.setData({
          totalDays,
          averageTomatoes
        })
        wx.hideLoading()
      }
    })
  }
})