Page({
  data: {
    // 计时条件
    keepTiming: true,

    // 音乐有声无声条件
    audioPlaying: true,
    audioSrc: 'http://music.163.com/song/media/outer/url?id=167876.mp3',
    canPlay: false,
    canPause: false,
    canStop: false,

    // 时长
    Duration: '',
    maxDuration: 25 * 60,
    minutes: 25,
    seconds: `00`,

    // 旋转角度
    leftDeg: `transform: rotate(180deg)`,
    rightDeg: `transform: rotate(-180reg)`,

    // 番茄数目
    getTomaNum:0,
    dayTomatoes: 0,
    totalTomatoes: 0,
    weekTomatoes: 0,
    averageTomatoes: 0,
    totalDays: 0,

    temp: {},
    selectedTask:'工作',
    taskIndex:0
  },
  audioCtx: wx.createInnerAudioContext('myAudio'),
  timingTimer:null,
  animationTimer:null,
  // 音频播放
  handlePlay(flag) {
    // 获取数据源中的audioSrc
    let audioSrc = this.data.audioSrc;
    let canPlay = this.data.canPlay;
    //播放
    if (canPlay && flag) {
      console.log('play');
      this.audioCtx.src = audioSrc;
      this.audioCtx.loop = true;
      this.audioCtx.useWebAudioImplement = true;
      this.audioCtx.play();
    }
  },
  //  音频暂停
  handlePause(flag) {
    let canPause = this.data.canPause;
    if (canPause == true && flag) {
      // InnerAudioContext.pause()
      // 暂停。 暂停后的音频再播放会从暂停处开始播放
      console.log('pause');
      this.audioCtx.pause();
    }
  },
  // 音频结束
  handleStop(flag) {
    console.log('stop');
    let canStop = this.data.canStop;
    if (canStop && flag) {
      // InnerAudioContext.stop()
      // 停止。 停止后的音频再播放会从头开始播放。
      this.audioCtx.stop();
    }
  },
  // 音频静音
  handleVolume(flag) {
    let audioPlaying = this.data.audioPlaying;
    if (!audioPlaying && !flag) {
      console.log('volume--');
      this.audioCtx.volume = 0;
    } else {
      console.log('volume++');
      this.audioCtx.volume = 1;
    }
  },
  // 喇叭触发器
  handlePlayAudio() {
    let audioPlaying = this.data.audioPlaying;
    audioPlaying = !audioPlaying
    this.setData({ //改变静音条件
      audioPlaying
    })
    if (!audioPlaying) {
      // 静音
      this.handleVolume(false);
    } else {
      this.handleVolume(true);
    }
  },
  // 开启新番茄或放弃当前任务
  handleRestart() {
    let canStop = this.data.canStop;
    canStop = !canStop;
    let canPlay = this.data.canPlay;
    canPlay = false;
    wx.switchTab({
      url: '/pages/timeKeeping/timeKeeping',
    });
    this.setData({ //改变结束音频播放的条件
      canStop,
      canPlay
    })
    if (canStop) {
      this.handleStop(true);
    }

  },
  // 暂停计时
  handlePauseTiming() {
    console.log('暂停计时 handlePauseTiming');
    let canPause = this.data.canPause;
    canPause = !canPause;
    this.setData({
      canPause
    })
    if (canPause) {
      // 音乐暂停播放
      this.handlePause(true);
      // 计时暂停(进度条也暂停)
      // this.handleTiming(false);
      this.PauseTimer(this.timingTimer, {
        timingTimer: this.timingTimer,
        maxDuration: this.data.maxDuration,
        minutes: this.data.minutes,
        seconds: this.data.seconds,
        leftDeg: this.data.leftDeg,
        rightDeg: this.data.rightDeg
      })
      console.log(this.data.temp, 'temp');
    }
  },
  // 继续计时
  handleContinueTiming() {
    let canPause = this.data.canPause;
    canPause = !canPause;
    let canPlay = this.data.canPlay;
    this.setData({
      canPause
    })
    if (!canPause) {
      canPlay = true;
      this.setData({
        canPlay
      })
      this.handlePlay(true);
      // 计时器继续计时
			this.ContinueTimer(this.timingTimer, this.data.temp)
    }
  },
  // 计时中
  handleTiming(flag) {
    let maxDuration = this.data.maxDuration;
    if (maxDuration >= 0) {
      let minutes = Math.floor(maxDuration / 60); //25
      let seconds = Math.floor(maxDuration % 60); //0
      if (parseInt(minutes / 10) == 0) { //格式化
        minutes = `0${minutes}`;
      }
      if (parseInt(seconds / 10) == 0) { //格式化
        seconds = `0${seconds}`;
      }
      --maxDuration;
      this.setData({
        maxDuration,
        minutes,
        seconds
      })
    } else {
      clearTimeout(this.timingTimer);
      let canStop = this.data.canStop;
      canStop = true;
      this.setData({
        canStop
      })
      this.handleStop(true);
      // let getTomaNum=this.data.getTomaNum;
      let dayTomatoes = this.data.dayTomatoes;
      let totalTomatoes = this.data.totalTomatoes;
      let nowTime=dayjs().format('YYYY-MM-DD')
      dayTomatoes += 1;
      totalTomatoes += dayTomatoes;
      let itemInfo={
        nowTime:nowTime,
        selectedTask:this.data.selectedTask,
        getTomaNum:1,
        duration:this.data.Duration
      }
      this.setData({
        keepTiming: false,
        getTomaNum:1,
        dayTomatoes,
        totalTomatoes
      })
      wx.cloud.callFunction({
        name: 'calculateTomatoes',
        data: {
          dayTomatoes: this.data.dayTomatoes,
          totalTomatoes: this.data.totalTomatoes,
          itemInfo:itemInfo
        },
        success: (res) => {
          console.log(res);
        }
      })
      return;
    }
    this.timingTimer = setTimeout(this.handleTiming, 1000);

    // 实现计时动画效果
    // 获取总时长
    let totalDuration = 25 * 60;
    let leftDeg = this.data.leftDeg;
    let rightDeg = this.data.rightDeg;
    // 定时器
   this.animationTimer = setTimeout(() => {
      // 计算旋转角度
      let rotateDeg = Number((totalDuration - maxDuration) / totalDuration * 360).toFixed(0);
      if (rotateDeg > 360) {
        clearTimeout(this.animationTimer);
      } else {
        if (rotateDeg > 180) {
          leftDeg = `transform:rotate(${rotateDeg}deg)`;
          this.setData({
            leftDeg
          })
        } else {
          rightDeg = `transform:rotate(-${180 - rotateDeg}deg)`;
          this.setData({
            rightDeg
          })
        }
      }
    }, 1000)
  },

  onShow() {
    let canPlay = this.data.canPlay;
    canPlay = !canPlay;
    this.handleTiming();
    this.setData({ //音乐播放条件
      canPlay
    })
    console.log('canPlay=',canPlay);
    if (canPlay) { //播放音乐
      this.handlePlay(true)
    }
  },
  onLoad(option){
    console.log(option);
    this.setData({
      selectedTask:option.task
    })
  },
  // 暂停计时器
  PauseTimer(timer, info) {
    this.data.temp = info
    clearTimeout(timer)
  },
  // 定时器继续执行
  ContinueTimer(timer, dataTemp) {
    this.data.maxDuration = dataTemp.maxDuration
    this.data.minutes = dataTemp.minutes
    this.data.seconds = dataTemp.seconds
    this.data.leftDeg = dataTemp.leftDeg
    this.data.rightDeg = dataTemp.rightDeg
   this.timingTimer = setTimeout(this.handleTiming, 1000)
  }
})