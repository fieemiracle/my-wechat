// 获取应用实例
const app = getApp();
// 引入封装函数
import {
  showModal
} from '../../utils/tools'
Page({
  data: {
    catalogue: [{
        item_id: 0,
        item_icon: "../../images/icon_work.png",
        item_title: "工作"
      },
      {
        item_id: 1,
        item_icon: "../../images/icon_study.png",
        item_title: "学习"
      },
      {
        item_id: 2,
        item_icon: "../../images/icon_thought.png",
        item_title: "思考"
      },
      {
        item_id: 3,
        item_icon: "../../images/icon_write.png",
        item_title: "写作"
      },
      {
        item_id: 4,
        item_icon: "../../images/icon_running.png",
        item_title: "运动"
      },
      {
        item_id: 5,
        item_icon: "../../images/icon_reading.png",
        item_title: "阅读",
        bottom_color: '#FF4258',
        audioSrc: 'http://music.163.com/song/media/outer/url?id=167876.mp3'


      }
    ],

    // 下边框颜色切换
    index: 0,
    itemId: 0,
    addClassName:'timeKeeping_work_active',
    classArr: [{
      id: 0,
      className: 'timeKeeping_work_active'
    }, {
      id: 1,
      className: 'timeKeeping_study_active'
    }, {
      id: 2,
      className: 'timeKeeping_thought_active'
    }, {
      id: 3,
      className: 'timeKeeping_write_active'
    }, {
      id: 4,
      className: 'timeKeeping_running_active'
    }, {
      id: 5,
      className: 'timeKeeping_reading_active'
    }],

    // 用户
    userInfo: {},
    hasUserInfo: false,
    canIUseGetUserProfile: false,

    // 任务类型
    task:'工作'
  },
  // 任务切换效果
  handleChangeItem(e) {
    // console.log(e.currentTarget.dataset.index);
    let taskIndex=e.currentTarget.dataset.index;
    console.log(e.currentTarget.dataset.index,this.data.catalogue[taskIndex].item_title);
    // 获取数据源中的catalogue和事件源中的itemId
    let catalogue = this.data.catalogue;
    let index = e.currentTarget.dataset.index;
    let itemId = catalogue[index].item_id;
    let classArr=this.data.classArr;
    let addClassName=this.data.addClassName;
    if(index===classArr[index].id){//当前点击的item下标与该项数组的id
      addClassName=classArr[index].className;
      // console.log(addClassName);
    }
    this.setData({
      index,
      itemId,
      addClassName,
      task:this.data.catalogue[taskIndex].item_title
    })
  },

  // 开始计时
  handleKeepTime() {
    // 跳转开始计时页面
    wx.navigateTo({
      url: `../timmingStart/timmingStart?task=${this.data.task}`
    })
  },
  onLoad() {
    // 用户登录
    wx.showModal({
      content: '获取授权',
      success: (res) => {
        // console.log(res);
        if (res.confirm) {
          wx.getUserProfile({
            desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
            success: (res) => {
              this.setData({
                userInfo: res.userInfo,
                hasUserInfo: true
              })
            },
            fail: () => {
              wx.showModal({
                content: '用户登录失败'
              })
            }
          });

          if (wx.getUserProfile) {
            this.setData({
              canIUseGetUserProfile: true
            })
          }
        } else if (res.cancel) {
          wx.showModal({
            content: '用户授权失败'
          })
        }
      }
    })
  }
})