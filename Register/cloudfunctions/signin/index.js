// 云函数入口文件
const cloud = require('wx-server-sdk')
const env = 'cloud1-4g42994n5a7bf42f';
cloud.init()
const db = cloud.database({
  env
});
// 云函数入口函数
exports.main = async (event, context) => {
  console.log(event);
  // console.log(event.username);
  // console.log(event.usersecret);

  // 判断用户名和密码是否正确是否已经存在
  // let username=event.username;
  // let usersecret=event.usersecret;
  const checkName = await db.collection('login-register').where({
    username: event.username
  }).get()

  const checkSecret = await db.collection('login-register').where({
    usersecret: event.usersecret
  }).get()

  return {
    event,
    checkName,
    checkSecret
  }

}