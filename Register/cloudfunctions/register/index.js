// 云函数入口文件
const cloud = require('wx-server-sdk')
const env = 'cloud1-4g42994n5a7bf42f';
cloud.init()
const db = cloud.database({
  env
});

// 云函数入口函数
exports.main = async (event, context) => {
  console.log(event);
  const userInfo = event.userInfo;

  const checkName = await db.collection('login-register').where({
    username: event.username
  }).get()
  if (checkName.data.length == 0) {
    // 关联集合
    return await db.collection('login-register').add({
      data: {
        username: event.username,
        usersecret: event.usersecret,
        useremail: event.useremail,
        // createBy: userInfo.openID,
        registerTime: new Date(),
        delete: false,
        updateTime: new Date()
      }
    }).then(res => {

      return {
        event,
        checkName
      }
    })
  }else{
    return {
      event,
      checkName
    }
  }



}