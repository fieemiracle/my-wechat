// promise形式 封装 showModal
export const showModal=({content})=>{
  return new Promise((resolve,reject)=>{
      wx.showModal({
        title:'提示',
        content:content,
          success: (result) => {
              resolve(result);
          },
          fail: (err) => {
              reject(err);
          }
      });
        
  })
}
// promise形式 封装 showToast
export const showToast=({title,icon})=>{
  return new Promise((resolve,reject)=>{
      wx.showToast({
        title: title,
        icon: icon,
          success: (result) => {
              resolve(result);
          },
          fail: (err) => {
              reject(err);
          }
      });
        
  })
}