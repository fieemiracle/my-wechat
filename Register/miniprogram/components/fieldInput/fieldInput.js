import {
  showModal
} from "../../utils/tools"
Component({
  properties: {
    inputIcon: {
      type: String,
      value: ''
    },
    placeHolder: {
      type: String,
      value: ''
    },
    bindfunction: {
      type: String,
      value: ''
    }
  },
  data: {},
  methods: {
    handleUserName(e) {
      // console.log(e.detail.value);
      let username = e.detail.value;
      if (username != '') {
        let reg = /(13|14|15|16|17|18|19)\d{9}$/;
        if (!reg.test(username)) {
          wx.showModal({
            title: '用户名只能为11位手机号',
            icon: 'error',
            mask: true
          })
        } else {
          this.triggerEvent('myevent', username);
        }
      } else {
        wx.showModal({
          title: '用户名不能为空',
          icon: 'fail',
          mask: true
        })
      }
    },
    handleUsersecret(e) {
      // console.log(e.detail.value);
      let usersecret = e.detail.value
      if (usersecret != ' ') {
        let reg = /\d{6}$/;
        if (!reg.test(usersecret)) {
          wx.showModal({
            title: '6位密码',
            icon: 'error',
            mask: true
          })
          return false;
        } else {
          this.triggerEvent('myevent', usersecret)
        }
      } else {
        wx.showModal({
          title: '密码不能为空',
          icon: 'error',
          mask: true
        })
      }
    },
    handleUseremail(e) {
      let useremail = e.detail.value;
      if (useremail != '') {
        let reg = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        if (!reg.test(useremail)) {
          wx.showModal({
            title: '邮箱错误',
            icon: 'error',
            mask: true
          })
        } else {
          this.triggerEvent('myevent', useremail)
        }

      } else {
        wx.showModal({
          title: '邮箱不能为空',
          icon: 'error',
          mask: true
        })
      }
    }
    // ,
    // onTap() {
    //   var myEventDetail = {
    //     'username': this.data.username,
    //     'usersecret': this.data.usersecret,
    //     'useremail': this.data.useremail
    //   } // detail对象，提供给事件监听函数
    //   var myEventOption = {} // 触发事件的选项
    //   this.triggerEvent('myevent', myEventDetail);
    //   console.log(myEventDetail);
    // }
  }
})