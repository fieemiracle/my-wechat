// pages/login/login.js
Page({
  data: {
    index: 0,
    username: '',
    usersecret: '',
    useremail: ''
  },
  // 页面跳转
  handleToregister() {
    this.setData({
      index: 1
    })
  },
  handleTologin() {
    this.setData({
      index: 0
    })
  },

  // 获取子组件传来的数据
  getUsername(res) {
    // console.log(res);
    let username = this.data.username;
    this.setData({
      username: res.detail
    })
    // console.log(this.data.username);
  },
  getUsersecret(res) {
    // console.log(res);
    let usersecret = this.data.usersecret;
    this.setData({
      usersecret: res.detail
    })
    // console.log(this.data.usersecret);
  },
  getUseremail(res) {
    // console.log(res);
    let useremail = this.data.useremail;
    this.setData({
      useremail: res.detail
    })
    // console.log(this.data.useremail);
  },

  // 处理注册成功页面
  handleRegister() {
    wx.showToast({
      title: '注册成功',
    })
  },
  // 处理登录成功页面
  handleLogin() {
    wx.showToast({
      title: '登录成功',
    })
    wx.switchTab({
      url: '/miniprogram/pages/mine/mine',
    })
  },

  // 将注册信息传入云函数
  handleRegist(e) {
    // const db = wx.cloud.database()
    let username = this.data.username;
    wx.cloud.callFunction({
      name: 'register',
      data: {
        username: this.data.username,
        usersecret: this.data.usersecret,
        useremail: this.data.useremail
      },
      success: (res) => {
        // console.log(res.result);
        // console.log(res.result.event.username);
        if (res.result.checkName.data.length === 0) { //说明数据库没有该用户名，允许注册
          this.handleRegister();
          this.setData({
            index: 0,
            username: '',
            usersecret: '',
            useremail: ''
          })
          this.handleTologin();
        }else{
          wx.showToast({
            title: '该账号已注册',
            icon:'error',
            mask:true
          })
          this.setData({
            username: '',
            usersecret: '',
            useremail: ''
          })
        }
      }
    })
  },

  // 将登录信息与数据库中注册信息比对
  handleLogin() {
    let username = this.data.username;
    let usersecret = this.data.usersecret;
    // const db = wx.cloud.database();
    wx.cloud.callFunction({
      name: 'signin',
      data: {
        username: this.data.username,
        usersecret: this.data.usersecret
      },
      success: (res) => {
        // console.log(res);
        let checkName = res.result.checkName.data;
        let checkSecret = res.result.checkSecret.data;
        // console.log(checkName);
        // console.log(checkSecret);
        if (checkName.length == 1 && checkSecret.length == 1) {
          if (username === checkName[0].username && usersecret === checkSecret[0].usersecret) {
            wx.showToast({
              title: '登录成功',
            })

            wx.navigateTo({
              url: '/miniprogram/pages/mine/mine.wxml',
            })
          } else {
            wx.showToast({
              title: '账号或密码错误',
              icon: none
            })
          }

          // wx.switchTab({
          //   url: '/miniprogram/pages/mine/mine.wxml',
          // })

        } else if (checkName.length == 1 || checkSecret.length == 1) {
          wx.showToast({
            title: '账号或密码错误',
            icon: 'error'
          })
        } else {
          wx.showModal({
            content: '账户不存在',
          })
        }
      },
      fail: (err) => {

      }
    })
  }
})