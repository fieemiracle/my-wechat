// 直接初始化云服务器
wx.cloud.init();
// const {$toast}=require ("../../utils/util.js")//使用node的方式引入
import { $toast } from "../../utils/util.js"//小程序前端引入方式
import request from "../../utils/request.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    noteContent: '',
    noteTitle: '',
    noteImg: '',
    fileList: [],
    show: false,
    actions: [
      {
        name: '美食'
      },
      {
        name: '旅行'
      },
      {
        name: '汽车'
      },
      {
        name: '科技'
      },
      {
        name: '时尚'
      },
    ],
    note_type: ''
  },
  saveNoteContent(e) {
    console.log(e);
    this.setData({
      noteContent: e.detail.html
    })
  },
  editNoteTitle(e) {
    console.log(e);
    this.setData({
      noteTitle: e.detail
    })
  },
  editNoteImage(e) {
    console.log(e);
    wx.cloud.uploadFile({
      // 云端在线地址可以自己定义
      cloudPath: `${new Date().getTime()}.png`,
      filePath: e.detail.file.url
    }).then(res => {
      console.log(res);
      let fileList = this.data.fileList;
      fileList.push({
        url: res.fileID
      })
      this.setData({
        noteImg: res.fileID,
        fileList
      })
    })
  },
  deleteNoteImage(e) {
    console.log(e);
    let fileList = this.data.fileList
    fileList.splice(e.detail.index, 1)

    this.setData({
      fileList,
      noteImg: ''
    })
  },
  onClose() {
    this.setData({
      show: false
    })
  },
  showAction() {
    this.setData({
      show: true
    })
  },
  onSelect(e) {
    console.log(e);
    this.setData({
      note_type: e.detail.name,
      show: false
    })
  },
  toPublishNote() {
    console.log(123);
    let { nickname, id } = wx.getStorageSync('userInfo')
    request('/diary/publishNote', 'post', {
      note_content: this.data.noteContent,
      title: this.data.noteTitle,
      head_img: this.data.noteImg,
      note_type: this.data.note_type,
      nickname: nickname,
      useId: id,
      c_time: `${new Date().getFullYear()}年${new Date().getMonth()}月${new Date().getDate()}日`,
      m_time: `${new Date().getFullYear()}年${new Date().getMonth()}月${new Date().getDate()}日`
    }).then(res => {
      console.log(res);
      $toast(res.data.mess, 'success');
      setTimeout(() => {
        wx.navigateTo({
          url: '../noteClass/noteClass',
        });
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})