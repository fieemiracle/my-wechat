// const {$toast}=require ("../../utils/util.js")//使用node的方式引入
import {$toast} from "../../utils/util.js"//小程序前端引入方式
import request from "../../utils/request.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
   noteList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // wx.getStorage({
    //   key: 'userInfo',
    //   fail: ()=>{
    //     console.log(123);
    //     wx.navigateTo({
    //       url: '../../login/login'
    //     });
    //   },
    // });

    // console.log(options);
    // 发接口请求
    request('/diary/findNoteListByType','post',{
     note_type:options.title
    }).then((res)=>{
      console.log(res);
      $toast(res.data.mess,'success');
      this.setData({
        noteList:res.data.data
      })
      // console.log(this.data.noteList);
    })
  },
  goNoteDetail(e){
    console.log(e);
    let id=e.currentTarget.dataset.id;
    wx.navigateTo({
      url: `../noteDetail/noteDetail?id=${id}`,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})