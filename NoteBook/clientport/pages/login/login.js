// const {$toast}=require ("../../utils/util.js")//使用node的方式引入
import {$toast} from "../../utils/util.js"//小程序前端引入方式
import request from "../../utils/request.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    username:'',
    password:''
  },
  getUsernameVal(e){
    // console.log(e.detail.value);
    this.setData({
      username:e.detail.value
    })
  },
  getPasswordVal(e){
    // console.log(e.detail.value);
    this.setData({
      password:e.detail.value
    })
  },
  // 登录
  handleLogin(){
    if(this.data.username.trim()==''|| this.data.password.trim()==''){
      $toast('账号或密码错误','error');
      return;
    }
    request('/user/userLogin','post',{
      username:this.data.username,
      password:this.data.password
    }).then((res)=>{
      // console.log(res);
      $toast(res.data.mess,'success');
      // 存入缓存
      wx.setStorage({
        key:'userInfo',
        data:res.data.data
      });
      // 跳转页面
      setTimeout(()=>{
        wx.navigateTo({
          url:'../noteClass/noteClass'
        });
      },1000)
    })
  },
  // 去注册页面
  toRegister(){
    wx.navigateTo({
      url: '../register/register',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})