// pages/noteClass/noteClass.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatar: '../../images/raw_1512446140.jpeg',
    username: 'wn',
    isClickMenu: false,
    noteClassList: [{
        img: "../../images/raw_1512446214.jpeg",
        title: "美食",
      },
      {
        img: "../../images/raw_1512446225.jpeg",
        title: "旅行",
      },
      {
        img: "../../images/raw_1512446234.jpeg",
        title: "汽车",
      },
      {
        img: "../../images/raw_1512446243.jpeg",
        title: "时尚",
      },
      {
        img: "../../images/raw_1512446251.jpeg",
        title: "科技",
      },
    ],
    // label:''
  },
  showMenu() {
    console.log('showMenu');
    this.setData({
      isClickMenu: true
    })
  },
  hideMenu() {
    console.log('hideMenu');
    this.setData({
      isClickMenu: false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _this=this;
    wx.getStorage({
      key: 'userInfo',
      success(res){
        // console.log(res.data);
        _this.setData({
          username:res.data.username
        })
        // console.log(_this.data.username);
      },
      fail: (err) => {
        wx.navigateTo({
          url: '../login/login'
        });
      }
    })
  },
  toNoteList(e){
    console.log(e.currentTarget.dataset.title);
    let title=e.currentTarget.dataset.title;
    
    wx.navigateTo({
      url: `../noteList/noteList?title=${title}`,
    })
  },
  toEditDiary(e){
    // console.log(e);
    wx.navigateTo({
      url: '../noteEdit/noteEdit'
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})