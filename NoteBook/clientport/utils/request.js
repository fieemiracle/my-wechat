const {
    $toast
  } = require("./util")
  
  function request(url, method, data = {}) {
    return new Promise((resolve, reject) => {
      wx.request({
        url: `http://localhost:3000${url}`,
        method,
        data,
        success: (res) => {
          if (res.data.code == 200) {
            resolve(res)
          } else {
            $toast(res.data.mess, 'error')
          }
  
        },
        fail: (err) => {
          reject(err)
        }
      })
    })
  
  }
  
  export default request