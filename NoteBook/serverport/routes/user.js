const router = require('koa-router')();
const userService=require('../controllers/mySqlConfig');

//当前js公共前缀
router.prefix('/user')

//登录接口
router.post('/userLogin', async (ctx, next) => {
    //拿到前端给我的参数
    let _username = ctx.request.body.username
    let _password = ctx.request.body.password

    // console.log(_username, _password);
    // 去数据库里面匹配
    // userLogin(_username, _password);
    await userService.userLogin(_username, _password)
    .then(res=>{
        console.log(res);
        if(res.length){//有值
            let result={
              id:res[0].id||1,
              nickname:res[0].nickname,
              username:res[0].username
              
            }
            ctx.body={
                code:200,
                data:result,
                mess:'登录成功'
            }
          }else{
            ctx.body={
                code:8005,
                data:'哎呀，账号/密码错误',
                mess:'账号或密码有误'
            }
          }
    })
    .catch(err=>{
        ctx.body={
            code:8005,
            data:err,
            mess:'开小差啦~'
        }
    });
})


// 注册接口
router.post('/userRegister', async(ctx, next)=>{
    // 拿到前端给我的参数
    let {username,password,nickname} = ctx.request.body;
    console.log(username,password,nickname);
    if(!username&&!nickname&&!password){
        ctx.body={
            code: 401,
            data:'信息不能为空',
            mess:'Invalid'
        }

    }

    // 先判断数据库有没有这个username
    await userService.findUser(username).then(async (res)=>{
        // console.log(res);
        if(res.length){
            ctx.body={
                code:'80003',
                mess:'账号已存在'
            }
        }else{//账号在数据库中不存在，允许注册
            await userService.insertUser([username,password,nickname])
            .then(res=>{
                // console.log(res);
                if(res.affectedRows){
                    ctx.body={
                        code:200,
                        data:'ok',
                        mess:'注册成功'
                    }
                }else{
                    ctx.body={
                        code:'80001',
                        data:'error',
                        mess:'注册失败'
                    }
                }
            })
            .catch(error=>{
                ctx.body={
                code:'80001', 
                data:'error', 
                mess:'开小差啦~'}
            })
        }
    })
})
module.exports = router