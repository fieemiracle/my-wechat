const Koa = require('koa');
const app = new Koa();
const bodyParser = require('koa-bodyparser');
const index = require('./routes/index'); //引入路由
const user = require('./routes/user');
const diary = require('./routes/diary');

//帮助koa解析post请求传递的参数
app.use(bodyParser())

app.use(index.routes(), index.allowedMethods()); //启用路由
app.use(user.routes(), user.allowedMethods()); //启用路由
app.use(diary.routes(), diary.allowedMethods()); //启用路由

app.listen(3000, () => {
    console.log('项目已启动');
});